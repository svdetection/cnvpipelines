

include: "common.smk"


cmd = CommandFactory.factory(config)

workdir: config['wdir']

rule all:
    input:
        cmd.get_all_outputs

include: "rules/refbundle.smk"
