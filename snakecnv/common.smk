import os
import sys
from termcolor import cprint
from collections import defaultdict
import traceback
import glob
import yaml
import pandas as pd
import pysam

# TODO shouldn't this be defined elsewhere ?
ROOTPATH = os.path.dirname(workflow.snakefile)
shell.prefix("export PATH=%s/bin:\"$PATH\";" % ROOTPATH)
os.environ["PYTHONPATH"] = os.path.join(ROOTPATH, "lib")

# For each tool the list of sv that can be detected
# FIXME organize this differently
include: "rules/tools/svtypes.smk"

"""
  Helper class and functions
"""

class CommandFactory(object):
    """
    The object factory design pattern
    https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Factory.html

    A single static method, factory, returning constructed objects according
    to type
    """

    def __init__(self):
        pass

    @staticmethod
    def factory(params):
        if "wf_type" not in params:
            print_error_exit("wf_type absent from config file")
        # return eval(type + "()") hence the corresponding object
        mode = params['wf_type']
        if mode == "align":
            return Align(params)
        if mode == "detection":
            return Detection(params)
        if mode == "simulation":
            return Simulation(params)
        if mode == "refbundle":
            return RefBundle(params)
        if mode == "mergebatches":
            return MergeBatches(params)
        print_error_exit("%s not a valid mode" % mode)


class WorkflowException(Exception):
    pass


class Command(object):
    """
    The virtual Command object.
    Stores information for a given workflow:
      - wdir, reference, samples, ...

    Attributes
    ----------
    params: dict
        the snakemake config dictionnary
    wdir: str
        the snakemake working directory
    reference: str
        the absolute path to the genome reference file
    samples: pd data frame
        a panda data frame storing the content of the sample file
    root_path: str
        the absolute path of the Snakefile file
    """

    def __init__(self, params):
        self.params = params

        set_absolute_path(params, "wdir")
        self._wdir = params["wdir"]

        # The reference absolute path
        self.set_reference_absolute_path(params)
        self.reference = params["reference"]

        self.root_path = os.path.dirname(workflow.snakefile)

    @property
    def get_reference(self):
        return self.reference

    @property
    def wdir(self):
        return self._wdir

    def get_root_path(self, wildcards):
            return self.root_path

    def set_reference_absolute_path(self, params):
        set_absolute_path(params, "reference")

    def get_param(self, key):
        if key not in self.params:
            print_error_exit("key %s absent from config file" % key)
        return self.params[key]

    def get_param_as_str(self, key):
        param = self.get_param(key)
        if isinstance(param, list):
            return " ".join(map(str, param))
        else:
            return str(param)

    def get_root_path(self, wildcards):
        return self.root_path

    def get_sample_fastq(self, wildcards):
        pass

    def get_all_outputs(self, wildcards):
        # Set all the output of the given workflow
        # input of the snakemake all rule
        pass


class Align(Command):
    """
    The Align Command object.

    """
    def __init__(self, params):
        Command.__init__(self, params)

        set_absolute_path(params, "sample_file")
        self.samples = self.set_samples_dataframe(params)
        Align.setabsolute_read_paths(self.samples)

    @staticmethod
    def setabsolute_read_paths(samples):
        absolutepaths = []
        for filepaths in samples.reads:
            files = ",".join([os.path.abspath(f) for f in filepaths.split(",")])
            absolutepaths.append(files)
        samples["_reads"] = samples["reads"]
        samples["reads"] = absolutepaths

    @property
    def get_samples(self):
        return self.samples.keys()

    def set_samples_dataframe(self,  params):
        # convert the sample structure in a panda dataframe
        samples = self.get_samples_align(params["sample_file"])
        sample_names = []
        reads = []
        for sample in samples:
            sample_names.append(sample)
            if "reads1" in samples[sample]:
                reads.append(",".join([samples[sample]["reads1"],
                                      samples[sample]["reads2"]]))
            else:
                reads.append(samples[sample]["reads"])
        data = {"sample": sample_names, "reads": reads}
        return pd.DataFrame(data, index=sample_names)

    def get_samples_align(self, sample_file):
        samples = {}
        with open(sample_file) as smples:
            try:
                samples = yaml.load(smples, Loader=yaml.FullLoader)
            except yaml.YAMLError:
                traceback.print_exc()
                raise Exception("Error: sample file is not a valid YAML file")

        if not isinstance(samples, dict):
            raise Exception("Error: sample file is not valid")

        for sample, files in samples.items():
            if not isinstance(sample, str):
                samples[str(sample)] = samples[sample]
                del samples[sample]
            if not isinstance(files, dict):
                raise Exception("Error: sample file is not valid")
            if "reads1" not in files and "reads" not in files:
                raise Exception("Invalid sample file: each sample must have a reads1 or a reads key")
            if "reads" in files and "reads1" in files:
                raise Exception("Invalid sample file: a sample can't have both reads and reads1 key")
            if "reads" in files:
                files["reads1"] = files["reads"]
                del files["reads"]
            for file in files:
                files[file] = os.path.abspath(files[file])
        return samples

    def get_sample_fastq(self, wildcards):
        files = self.samples.loc[wildcards.sample]["reads"]
        if len(files.split(',')) > 1:
            fastq = files.split(',')
        else:
            fastq = files
        return {'fastq': fastq}

    def get_read_group(self, wildcards):
        " Denote sample name and platform in read group. "
        return r"'@RG\tID:{sample}\tLB:{sample}\tPL:ILLUMINA\tSM:{sample}'".format(
            sample=wildcards.sample)

    def get_all_outputs(self, wildcards):
        return expand("bams/{sample}.bam", sample=self.samples.index)


class Detection(Command):
    def __init__(self, params):
        Command.__init__(self, params)

        # now set absolute paths for bams
        # WARNING removed in order to be compliant with cnvpipelines.py
        # WARNING wich set the bam files in the working dir
        # Detection.setabsolute_bam_paths(self.samples)

        set_absolute_path(params, "sample_file")
        self.samples = self.set_samples_dataframe(params)

        self.chromosomes = self.params['chromosomes']
        varianttypes = ['DEL', 'INV', 'DUP', 'mCNV']
        if "varianttypes" in self.params:
            varianttypes = self.params["varianttypes"]
        self.varianttypes = varianttypes

        self.tools = self.params['tools']
        self.tools_sv_types = self.set_svtype_tools()

        # FIXME this is only a patch
        if "start_batch_nb" in self.params:
            start_batch_nb = self.params["start_batch_nb"]
        else:
            start_batch_nb = 1

        self.size_batches = self.get_size_batches()
        self.sample_batches = self.set_sample_batches(start_batch_nb)
        # self.chrom_batches = self.get_all_chrom_batches()

        # TODO have to clean this
        self.empty_template = os.path.join(self.root_path, "lib", "svreader",
                                           "resources", "template.vcf")

    @staticmethod
    def setabsolute_bam_paths(samples):
        absolutepaths = []
        for relative_path in samples.bam:
            abs_path = os.path.abspath(relative_path)
            absolutepaths.append(abs_path)
        samples["_bam"] = samples["bam"]
        samples["bam"] = absolutepaths

    @property
    def get_samples(self):
        return self.samples.index

    @property
    def get_chromosomes(self):
        return self.chromosomes

    @property
    def get_tools(self):
        return self.tools

    @property
    def get_template(self):
        return self.empty_template

    @property
    def get_batches(self):
        return [batch for batch in self.sample_batches]

    def get_batch_samples(self, batch):
        return self.sample_batches[batch]

    def get_read_group(self, wildcards):
        print("get_read_group should'nt be called with detectino command")
        exit(1)

    def get_sample_bam(self, sample):
        bam = self.samples.loc[sample]["bam"]
        return bam

    def set_samples_dataframe_old(self, params):
        return pd.read_table(params["sample_file"]).set_index("sample",
                                                              drop=False)

    def set_samples_dataframe(self, params):
        with open(params["sample_file"], "r") as fin:
            samples = [line.rstrip() for line in fin]
        bams = []
        for sample in samples:
            bamfile = "data/bams/%s.bam" % sample
            bams.append(bamfile)
        data = {'sample': samples, 'bam': bams}
        return pd.DataFrame(data, index=samples)

    def get_all_outputs(self, wildcards):
        outputs = expand("{batch}/filtered/{svtype}/svtyper_{chrom}_{svtype}_genotypes_filtered.vcf.gz",
                         chrom=self.chromosomes,
                         svtype=[x for x in self.varianttypes if x != "mCNV"],
                         batch=list(self.sample_batches.keys()))
        return outputs

    def get_input_bams(self, wildcards):
        inputs = []
        indexes = []
        for sample in self.sample_batches[wildcards.batch]:
            bam = self.get_sample_bam(sample)
            inputs.append(bam)
            indexes.append(bam + ".bai")
        return {"bams": inputs, "idxs": indexes}

    def dump_batch_bamlist(self, batch, outputfile):
        fout = open(outputfile, "w")
        for sample in self.sample_batches[batch]:
            bamfile = "data/bams/%s.bam" % sample
            fout.write(bamfile + "\n")
        fout.close()

    def set_sample_batches(self, start_batch_nb=1):
        samples = defaultdict()
        samples_batch = []
        for sample in self.samples.index:
            samples_batch.append(sample)
            if len(samples_batch) == self.size_batches:
                samples["batch%03d" % start_batch_nb] = samples_batch
                samples_batch = []
                start_batch_nb += 1
        if len(samples_batch) > 0:
            samples["batch%03d" % start_batch_nb] = samples_batch

        return samples

    def get_pindel_chrom_batches(self, wildcards):
        return self._get_pindel_chrom_batches(wildcards, self.get_reference)

    def _get_pindel_chrom_batches(self, wildcards, reference):
        inputs = {"D": [],
                  "INV": [],
                  "TD": []}

        # We define here the chromosome batches
        chrom_batches = self.get_chrom_batches(wildcards.chrom,
                                               reference,
                                               chr_batchsize=5000000,
                                               overlap=50000)
        for batch in chrom_batches:
            for svtype in inputs:
                inputs[svtype].append("{batch}/pindel/{chrom}/{chrbatch}/pindel_{chrom}_{svtype}.gz".format(
                    batch=wildcards.batch, chrom=wildcards.chrom, chrbatch="-".join(map(str, batch)),
                    svtype=svtype
                ))
        return inputs

    def get_size_batches(self):
        if "batches" in self.params:
            return int(self.params["batches"])
        else:
            return 1

    def get_first_bam(samples):
        return "data/bams/%s.bam" % ([x for x in samples.values()][0][0])

    def get_excluded(config):
        if ("excludebed" in self.params and
                os.isfile(self.params['excludebed'])):
            return config['excludebed']
        else:
            return ""

    def get_chrom_batches(self, chrom, reference,
                          chr_batchsize=5000000, overlap=50000):
        if chrom not in self.get_chromosomes:
            print_code_error_exit("%s is not a valid (selected) chromosome")

        fa = pysam.FastaFile(reference)
        len_chr = len(fa.fetch(chrom))

        # Make ranges:
        groups = []
        start = 1
        end = min(start + chr_batchsize - 1,  len_chr)
        while ((len_chr - end + 1 >= chr_batchsize - overlap) or
               (len_chr + 1 == end)):
            groups.append((start, end))
            if len_chr + 1 == end:
                break
            start = end - overlap + 1
            end = min(start + chr_batchsize - 1, len_chr + 1)
        last_group = (start, len_chr + 1)
        if last_group not in groups:
            groups.append(last_group)
        return groups

    def get_all_exclusion_files(self, wildcards):
        return ["exclusion/excluded_{sample}.bed" % sample for sample in self.get_all_samples() ]

    @staticmethod
    def get_delly_suffix(svtype):
        return "_" + svtype + ".bcf"

    @staticmethod
    def get_lumpy_suffix(svtype):
        return ".vcf.gz"

    @staticmethod
    def get_genomestrip_suffix(svtype):
        if svtype != "DEL":
            print("Genomestrip works only for deletions")
            exit(1)
        return ".vcf.gz"

    @staticmethod
    def get_pindel_suffix(svtype):
        SV_TYPE_TO_PINDEL = {
            "DEL": "D",
            "DUP": "TD",
            "INV": "INV",
            "RPL": "RPL"}
        if svtype not in SV_TYPE_TO_PINDEL:
            raise KeyError('Unsupported variant type')
        pindel_type = SV_TYPE_TO_PINDEL[svtype]
        outfile = "_" + pindel_type + ".gz"
        return outfile

    def get_tool_prefix(self, tool, chrom, svtype):
        common_prefix = "_".join([tool, chrom])
        if tool == "pindel":
            tool_suffix = self.get_pindel_suffix(svtype)
        elif tool == "lumpy":
            tool_suffix = self.get_lumpy_suffix(svtype)
        elif tool == "delly":
            tool_suffix = self.get_delly_suffix(svtype)
        elif tool == "genomestrip":
            tool_suffix = self.get_genomestrip_suffix(svtype)
        return common_prefix + tool_suffix



    def get_tool_ouput_file(self, wildcards):
        tool_prefix = self.get_tool_prefix(wildcards.tool,
                                           wildcards.chrom, wildcards.svtype)
        return {'tooloutput': os.path.join(wildcards.batch, wildcards.tool,
                                           tool_prefix)}

    def get_tools_output_for_svtype(self, wildcards):
        inputs = []
        for tool in self.tools:
            if wildcards.svtype in self.tools_sv_types[tool]:
                inputs.append("{batch}/parse/{svtype}/{tool}/{tool}_{chrom}_{svtype}_parsed.vcf.gz".format(
                    tool=tool, chrom=wildcards.chrom, svtype=wildcards.svtype, batch=wildcards.batch
                ))
        return inputs

    def set_svtype_tools(self):
        tool_sv_types = defaultdict()
        for tool in self.tools:
            sv_key = tool + "_sv"
            if sv_key in globals():
                tool_sv_types[tool] = globals()[sv_key]
            else:
                tool_sv_types[tool] = self.varianttypes
        return tool_sv_types


class RefBundle(Command):
    # master class Command defined in common/SnakemakeCommand.smk
    def __init__(self, params):
        Command.__init__(self, params)

    def get_all_outputs(self, wildcards):
        outputs = ["reference.fasta.fai",
                   "reference.svmask.fasta.fai",
                   "reference.gcmask.fasta.fai",
                   "reference.svmask.fasta.fai",
                   "reference.lcmask.fasta.fai",
                   "reference.rdmask.bed",
                   "reference.dict",
                   "reference.ploidymap.txt",
                   "reference.gendermask.bed",
                   "reference.Nstretch.bed"]
        return outputs


def input_is_empty(input_vcf):
    is_empty = True
    vcf = pysam.VariantFile(input_vcf)
    # Check if is empty:
    for record in vcf:
        is_empty = False
        break
    return is_empty


class MergeBatches(Detection):
    """
    The MergeBatches Command object.
      - merging the variants detected in the different batches in a single set
       of detected and genotyped variants for all individuals

    """
    def __init__(self, params):
        Detection.__init__(self, params)

    def get_all_outputs(self, wildcards):
        outputs = expand("annotated/{svtype}/svtyper_{chrom}_{svtype}_final.vcf.gz",
                          chrom=self.chromosomes,
                          svtype=[x for x in self.varianttypes if x != "mCNV"])
        concat = expand("results/{svtype}/cnvpipeline_{svtype}.vcf.gz", svtype=[x for x in self.varianttypes if x != "mCNV"])
        jupyter = ["results/detection_summary.html"]

        return outputs + concat + jupyter

    def get_annotated_outputs(self, wildcards):
        outputs = expand("annotated/{svtype}/svtyper_{chrom}_{svtype}_final.vcf.gz",
                          chrom=self.chromosomes,
                          svtype=[x for x in self.varianttypes if x != "mCNV"])
        return outputs


    def get_concat_input(self, wildcards):
        inputs = []
        for chrom in cmd.chromosomes:
            vcf_file = "annotated/%s/svtyper_%s_%s_final.vcf.gz" % (wildcards.svtype, chrom, wildcards.svtype)
            if not input_is_empty(vcf_file):
                inputs.append(vcf_file)
        return inputs

    def get_batches(self):
        """
        get batches, chromosomes and variant types from file hierarchy
        :param wdir: working directory
        """
        batch_re = re.compile(r"^batch\d+$")
        batches = []
        chromosomes = []
        variant_types = []
        wdir = cmd.wdir
        # Search batch files:
        for file in os.listdir(wdir):
            if re.match(batch_re, file):
                # It's a batch folder (check it next)
                filter_dir = os.path.join(wdir, file, "filtered")
                if os.path.isdir(filter_dir):

                    # Search for variants types:
                    my_variants = os.listdir(filter_dir)
                    if len(my_variants) == 0:
                        raise WorkflowException("Filtered folder of batch %s does not containes variants folders!" % file)
                    if len(variant_types) == 0:
                        variant_types = my_variants
                    else:
                        if len(variant_types) != len(my_variants):
                            raise WorkflowException("All batches does not have the same variants for filtered results!")
                        for variant in my_variants:
                            if variant not in ALLOWED_SV:
                                raise WorkflowException("Invalid variant type: %s" % type_v)
                            if variant not in variant_types:
                                raise WorkflowException("All batches does not have the same variants for filtered results!")
                            my_variant_types = []

                            # Search for chromosomes:
                            my_chrs = []
                            for ffile in glob(os.path.join(filter_dir, variant, "svtyper_*_%s_genotypes_filtered.vcf.gz" % variant)):
                                # chrm = ffile.split("_")[1] doesn't work when the directory path contains an "_"
                                basefilename = os.path.basename(ffile)
                                chrm = basefilename.split("_")[1]
                                my_chrs.append(chrm)

                            if len(my_chrs) == 0:
                                raise WorkflowException("Filtered folder of batch %s does not contains files for variant %s!" % (file, variant))
                            if len(chromosomes) == 0:
                                chromosomes = my_chrs
                            else:
                                if len(my_chrs) != len(chromosomes):
                                    raise WorkflowException("All batches or all variants does not have the same chromosomes for filtered results!")
                                for my_chr in my_chrs:
                                    if my_chr not in chromosomes:
                                        raise WorkflowException("All batches or all variants does not have the same chromosomes for filtered results!")

                batches.append(file)
        return batches, chromosomes, variant_types


class Simulation(Detection):
    def __init__(self, params):

        self._nb_inds = params["nb_inds"]
        Detection.__init__(self, params)

        self.popsim_outdir = "pop"

        # Used for get_pindel_chrom_batches in the absence of reference fasta
        self.reference_raw = "reference_raw.fasta"

    @property
    def nb_inds(self):
        return self._nb_inds

    def get_pindel_chrom_batches(self, wildcards):
        # PATCH : using here reference_raw.fasta
        # in the absence of reference.fasta
        return self._get_pindel_chrom_batches(wildcards, self.reference_raw)

    def set_reference_absolute_path(self, params):
        # we overload this function for the specific situation of
        # simulation where the reference genome is expected in the
        # working dir : reference.fasta
        params["_reference"] = params["reference"]

    def set_samples_dataframe(self, params):
        samples = {"sample": ["indiv%d" % i for i in range(1, self._nb_inds+1)]}
        return pd.DataFrame(samples, index=samples["sample"])

    def get_popsim_outdir(self):
        return self.popsim_outdir

    def get_read_group(self, wildcards):
        " Denote sample name and platform in read group. "
        return r"'@RG\tID:{sample}\tLB:{sample}\tPL:ILLUMINA\tSM:{sample}'".format(
            sample=wildcards.sample)

    def get_all_outputs(self, wildcards):
        outputs = []
        outputs.append("results/simulation_summary.ipynb")
        outputs.append("results/simulation_summary.html")
        return outputs

    def get_filtered_outputs(self, wildcards):
        outputs = expand("{batch}/filtered/{svtype}/svtyper_{chrom}_{svtype}_genotypes_filtered.vcf.gz",
                          chrom=self.chromosomes,
                          svtype=[x for x in self.varianttypes if x != "mCNV"],
                          batch=list(self.sample_batches.keys()))
        return outputs


    def get_buildpop_fastqs(self):
        # FIXME here the samples.yml constructed by cnvpipelines.
        outputs = []
        for s in self.get_samples:
            outputs += [temp(os.path.join(self.popsim_outdir, "%s_1.fq.gz" % s)),
                        temp(os.path.join(self.popsim_outdir, "%s_2.fq.gz" % s))]
        return outputs

    def get_buildpop_fastas(self):
        # FIXME here the samples.yml constructed by cnvpipelines.
        outputs = []
        for s in self.get_samples:
            outputs += [temp(os.path.join(self.popsim_outdir, "%s_chr_0.fasta" % s)),
                        temp(os.path.join(self.popsim_outdir, "%s_chr_1.fasta" % s))]
        return outputs

    def get_sample_fastq(self, wildcards):
        fastq = [os.path.join(self.popsim_outdir, "%s_1.fq.gz" % wildcards.sample),
                 os.path.join(self.popsim_outdir, "%s_2.fq.gz" % wildcards.sample)]
        return {'fastq': fastq}

    def get_sample_bam(self, sample):
        return "data/bams/%s.bam" % sample

    def buildinfilesresults(self, wildcards):
        genotypes = expand("{batch}/filtered/{{svtype}}/svtyper_{chrom}_{{svtype}}_genotypes_filtered.vcf.gz",
                          chrom=self.chromosomes,
                          batch=self.sample_batches.keys())
        filtered = expand("{batch}/filtered/{{svtype}}/svtyper_{chrom}_{{svtype}}_genotypes_filtered.vcf.gz",
                          chrom=self.chromosomes,
                          batch=self.sample_batches.keys())
        return {"genotypes": genotypes, "filtered": filtered}


def set_absolute_path(params, key):
        params["_"+key] = params[key]
        params[key] = os.path.abspath(params["_"+key])


def print_error_exit(message):
    cprint("WARNING: " + message + ", exiting softly!", 'magenta',
           attrs=['bold'], file=sys.stderr)
    os._exit(1)


def print_code_error_exit(message):
    cprint("WARNING: " + message + ", exiting softly!", 'red',
           attrs=['bold'], file=sys.stderr)
    os._exit(1)


def get_threads(rule, default):
    cluster_config = snakemake.workflow.cluster_config
    if rule in cluster_config and "threads" in cluster_config[rule]:
        return cluster_config[rule]["threads"]
    if "default" in cluster_config and "threads" in cluster_config["default"]:
        return cluster_config["default"]["threads"]
    return default


def which(program):
    import os

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None
