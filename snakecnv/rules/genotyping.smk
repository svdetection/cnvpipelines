

def get_batch_bams(wildcards):
    bam_infos = snakemake_utils.get_inputs_bams(wildcards)
    return ",".join(bam_infos['bams'])


rule svtyping:
    input:
        sites = "{batch}/merge/{svtype}/merge_{chrom}_{svtype}.vcf.gz",
        bamlist = "{batch}/bamlist.list",
        genome = REFERENCE,
        fai = REFERENCE + ".fai"
    output:
        vcf = "{batch}/genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes.vcf.gz"
    wildcard_constraints:
        batch = "\w+"
    threads:
        get_threads("svtyping", 4)
    log:
        stdout = "{batch}/logs/svtyper/{svtype}/{chrom}_svtyper.o",
        stderr = "{batch}/logs/svtyper/{svtype}/{chrom}_svtyper.e"
    shadow: "shallow"  # in order to prevent raw.vcf to be corrupted
    conda:
        "../envs/lumpysvtyperpy27.yaml"
    shell:
        "svtyper_launcher.py -B {input.bamlist} -i {input.sites} -o {output.vcf} -t {threads} "
        " 1>{log.stdout} 2>{log.stderr}"
