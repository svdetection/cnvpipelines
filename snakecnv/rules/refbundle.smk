

include: "refbundleutils.smk"

config = check_and_set_params(config)

SUBDIR = config["rb_subdir"] + os.path.sep if "rb_subdir" in config else ""

rule get_fasta:
    input:
        fasta = ancient(SUBDIR + "reference_raw.fasta"),
        index = ancient(SUBDIR + "reference_raw.fasta.fai")
    output:
        SUBDIR + "reference.fasta"
    params:
        chromosomes = config['chromosomes']
    shell:
        "samtools faidx {input.fasta} {params.chromosomes} > {output}; "
        "samtools faidx {output}"

rule preparesvmask:
    input:
        fasta = SUBDIR + "reference.fasta",
        fai = SUBDIR + "reference.fasta.fai"
    output:
        fasta = SUBDIR + "work/reference.fasta",
        index = SUBDIR + "work/reference.fasta.bwt"
    log:
        SUBDIR + "logs/preparesvmask.log"
    run:
        # function defined in commonrefbundle.sml
        svmask_index(input.fasta, output.fasta)


rule mergesvmask:
    input:
        expand(SUBDIR + "work/svmask_{chrom}.fasta",
               chrom=config['chromosomes'])
    output:
        SUBDIR + "reference.svmask.fasta"
    shell:
        "cat {input} > {output}"

rule chromsvmask:
    input:
        SUBDIR + "work/reference.fasta.bwt",
        fa = SUBDIR + "work/reference.fasta"
    output:
        SUBDIR + "work/svmask_{chrom}.fasta"
    params:
        rl = config['read_len'],
        classpath = config['CLASSPATH'],
        sv_dir = config['SV_DIR'],
        gs_bwa_dir = config['GS_BWA_DIR']
    log:
        SUBDIR + "logs/svmask/{chrom}.log"
    shell:
        "export PATH={params.gs_bwa_dir}:\"$PATH\"; "
        " export LD_LIBRARY_PATH={params.gs_bwa_dir}:\"$LD_LIBRARY_PATH\";"
        " java -cp {params.classpath}"
        " -Xmx4g org.broadinstitute.sv.apps.ComputeGenomeMask"
        " -R {input.fa} -O {output}"
        " -readLength {params.rl}"
        " -sequence {wildcards.chrom} 2> {log} "

rule index:
    input:
        SUBDIR + "{type}.fasta"
    output:
        SUBDIR + "{type}.fasta.fai"
    log:
        SUBDIR + "logs/index/{type}.log"
    shell:
        "samtools faidx {input} &> {log}"


rule bed2fasta:
    input:
        SUBDIR + "reference.fasta.fai",
        ref = SUBDIR + "reference.fasta",
        bed = SUBDIR + "{type}.bed"
    output:
        SUBDIR + "{type}.fasta"
    wildcard_constraints:
        type = ".*(lc|sv|gc)mask"
    params:
        classpath = config['CLASSPATH']
    shell:
        " java -cp {params.classpath} -Xmx4g "
        "     org.broadinstitute.sv.apps.BedFileToGenomeMask "
        "    -I {input.bed} "
        "    -R {input.ref}  "
        "    -O {output}"

rule repeatmask:
    input:
        fasta = SUBDIR + "reference.fasta",
        fai = SUBDIR + "reference.fasta.fai"
    output:
        SUBDIR + "reference.fasta.out"
    params:
        sp = config['species'],
        perl5lib = get_conda_perl5lib()
    threads:
        get_threads("repeatmask", 12)
    conda:
        "../envs/repeatmasker.yaml"
    shell:
        """
        export PERL5LIB={params.perl5lib} ;
        RepeatMasker -pa {threads} -species \"{params.sp}\" -xsmall {input.fasta}
         > {input.fasta}.repeatmasker.log
        """

rule length:
    input:
        fasta = SUBDIR + "reference.fasta",
        fai = SUBDIR + "reference.fasta.fai"
    output:
        SUBDIR + "reference.fasta.length"
    shell:
        "fastalength {input.fasta} > {output}"


rule rdmask:
    input:
        length = SUBDIR + "reference.fasta.length"
    output:
        bed = SUBDIR + "reference.rdmask.bed"
    run:
        with open(input.length) as f:
            with open(output.bed, "w") as fout:
                fout.write("\t".join(["CHROM", "START", "END"])+"\n")
                for line in f:
                    line = line.rstrip()
                    fields = line.split()
                    fout.write("\t".join([fields[1], "0",
                                          str(int(fields[0])-1)]) + "\n")

rule lcmaskbed:
    input:
        repeats = SUBDIR + "reference.fasta.out"
    output:
        bed = SUBDIR + "reference.lcmask.bed"
    run:
        repeats = []
        with open(input.repeats) as f:
            for line in f:
                if p_repeats.match(line):
                    fields = line.rstrip().split()
                    # append chrom start and end
                    repeats.append([fields[4], fields[5], fields[6]])
        sorted_repeats = sorted(repeats,
                                key=lambda x: (x[0], int(x[1]), int(x[2])))
        # Write to file
        import csv
        with open(output.bed, 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            writer.writerows(sorted_repeats)


rule gcmaskbed:
    input:
        repeats = SUBDIR + "reference.fasta.out"
    output:
        bed = SUBDIR + "reference.gcmask.bed"
    run:
        repeats = []
        with open(input.repeats) as f:
            for line in f:
                if p_repeats_all.match(line):
                    fields = line.rstrip().split()
                    repeats.append([fields[4], fields[5], fields[6]])
        sorted_repeats = sorted(repeats,
                                key=lambda x: (x[0], int(x[1]), int(x[2])))

        # Write to file
        import csv
        with open(output.bed, 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            writer.writerows(sorted_repeats)


rule dict:
    input:
        fasta = SUBDIR + "reference.fasta",
        fai = SUBDIR + "reference.fasta.fai"
    output:
        SUBDIR + "reference.dict"
    shell:
        "picard CreateSequenceDictionary "
        " R={input.fasta} "
        " O={output} "

rule nstretches:
    input:
        fasta = SUBDIR + "reference.fasta",
        fai = SUBDIR + "reference.fasta.fai"
    output:
        SUBDIR + "reference.Nstretch.bed"
    params:
        nstrech = config['maxNstretches']
    log:
        SUBDIR + "logs/nstretch.log"
    threads:
        1
    shell:
        "findNstretches.py -f {input.fasta} -m {params.nstrech} "
        "-o {output} -t {threads} 2> {log}"

rule ploidy:
    output:
        SUBDIR + "reference.ploidymap.txt"
    shell:
        " printf \"*\t*\t*\t*\t2\n\" > {output}"

rule gendermask:
    output:
        SUBDIR + "reference.gendermask.bed"
    shell:
        " touch {output}"
