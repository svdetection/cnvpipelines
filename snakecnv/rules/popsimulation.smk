import os

checkpoint get_fasta_popsim:
    input:
        fasta = "reference_raw.fasta",
        index = "reference_raw.fasta.fai"
    output:
        "reference.fasta",
        "reference.fasta.fai"
    params:
        chromosomes = config['chromosomes']
    shell:
        "samtools faidx {input.fasta} {params.chromosomes} > {output}; "
        "samtools faidx {output}"


rule buildpop:
    input:
        reference = cmd.get_reference,
        nstretches = "exclusion/gaps.bed"
    output:
        cmd.get_buildpop_fastqs(),
        cmd.get_buildpop_fastas(),
        genotype = "pop/genotypes.vcf.gz",
        yamlconf = temp("buildpop_params.yaml"),
    params:
        outdir = "pop"
    threads:
        get_threads("buildpop", 8)
    log:
        stdout = "logs/popsim.o",
        stderr = "logs/popsim.e"
    run:
        import yaml
        with open(output.yamlconf, 'w') as outfile:
            yaml.dump(config, outfile, default_flow_style=False)
        command = ["buildpop_launcher.py",
                   "--reference %s" % input.reference,
                   "--conf-file %s" % output.yamlconf,
                   "--nstretches %s" % input.nstretches,
                   "--outdir %s" % params.outdir,
                   "--threads %s" % threads]
        shell(" ".join(command) + " 1> %s 2> %s" % (log.stdout, log.stderr))


rule linkdatabams:
    input:
        bam = "bams/{sample}.bam",
        bai = "bams/{sample}.bam.bai"
    output:
        bam = "data/bams/{sample}.bam",
        bai = "data/bams/{sample}.bam.bai"
    run:
        os.symlink(os.path.abspath(input.bam), output.bam)
        os.symlink(os.path.abspath(input.bai), output.bai)


rule buildsimulationresults:
    input:
        unpack(cmd.get_filtered_outputs),
        genotype = "pop/genotypes.vcf.gz"
    output:
        jupyter = "results/simulation_summary.ipynb",
        html = "results/simulation_summary.html"
    threads:
        1
    params:
        path = cmd.get_root_path
    log:
        stdout = "logs/simulation_results.o",
        stderr = "logs/simulation_results.e"
    shell:
        """
            if [ ! -d results ]; then
                mkdir results
            fi
            cp {params.path}/simulation_summary.ipynb {output.jupyter}
            jupyter nbconvert --execute --to html {output.jupyter}
        """
