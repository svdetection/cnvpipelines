from svrunner_utils import getChromLength

cnvpipeline_sv = ['mCNV']


def intervallist(reference, chrom, output):
    chrom_length = getChromLength(reference, chrom)
    chrlistFh = open(output, "w")
    chrlistFh.write(chrom + ":" + str(1) + "-" + str(chrom_length) + "\n")
    chrlistFh.close()


rule intervallist:
    input:
        reference = REFERENCE
    output:
        intervallist = "cnvpipeline/{chrom}/interval.list"
    run:
        intervallist(input.reference, wildcards.chrom, output.intervallist)

rule cnvpipeline:
    input:
        unpack(snakemake_utils.get_inputs_bams),
        bamlist = "{batch}/bamlist.list",
        gendermap = "{batch}/genomestrip/gendermap.txt",
        metadatadone = "{batch}/genomestrip/metadata/sample_gender.report.txt",
        intervallist = "cnvpipeline/{chrom}/interval.list",
        genome = config['refbundle'] if 'refbundle' in config else REFERENCE
    params:
        metadata = "{batch}/genomestrip/metadata"
    output:
        "{batch}/cnvpipeline/{chrom}/cnvpipeline_{chrom}.vcf.gz"
    log:
        stdout="{batch}/logs/cnvpipeline/{chrom}.o",
        stderr="{batch}/logs/cnvpipeline/{chrom}.e"
    shell:
        "cnvpipeline.py --bamlist {input.bamlist} --reference {input.genome} --metadata {params.metadata} "
        "--gendermap {input.gendermap} --outdir {wildcards.batch}/cnvpipeline/{wildcards.chrom} "
        "--outprefix cnvpipeline_{wildcards.chrom} --interval-list {input.intervallist} 1> {log.stdout} 2> {log.stderr}"
