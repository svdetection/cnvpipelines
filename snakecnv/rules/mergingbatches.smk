import sys
import re
import os
from glob import glob
import shutil

from os.path import join, isfile
import subprocess

ALLOWED_SV = ("DEL", "INV", "DUP")

# TODO organize this elswhere and in a more readdable format
def get_batches(wdir):
    """
    get batches, chromosomes and variant types from file hiearchy
    :param wdir: working directory
    """
    batch_re = re.compile(r"^batch\d+$")
    batches = []
    chromosomes = []
    variant_types = []
    # Search batch files:
    for file in os.listdir(wdir):
        if re.match(batch_re, file):
            # It's a batch folder (check it next)
            filter_dir = os.path.join(wdir, file, "filtered")
            if os.path.isdir(filter_dir):

                # Search for variants types:
                my_variants = os.listdir(filter_dir)
                if len(my_variants) == 0:
                    raise WorkflowException("Filtered folder of batch %s does not containes variants folders!" % file)
                if len(variant_types) == 0:
                    variant_types = my_variants
                else:
                    if len(variant_types) != len(my_variants):
                        raise WorkflowException("All batches does not have the same variants for filtered results!")
                    for variant in my_variants:
                        if variant not in ALLOWED_SV:
                            raise WorkflowException("Invalid variant type: %s" % type_v)
                        if variant not in variant_types:
                            raise WorkflowException("All batches does not have the same variants for filtered results!")
                        my_variant_types = []

                        # Search for chromosomes:
                        my_chrs = []
                        for ffile in glob(os.path.join(filter_dir, variant, "svtyper_*_%s_genotypes_filtered.vcf.gz" % variant)):
                            # chrm = ffile.split("_")[1] doesn't work when the directory path contains an "_"
                            basefilename = os.path.basename(ffile)
                            chrm = basefilename.split("_")[1]
                            my_chrs.append(chrm)

                        if len(my_chrs) == 0:
                            raise WorkflowException("Filtered folder of batch %s does not contains files for variant %s!" % (file, variant))
                        if len(chromosomes) == 0:
                            chromosomes = my_chrs
                        else:
                            if len(my_chrs) != len(chromosomes):
                                raise WorkflowException("All batches or all variants does not have the same chromosomes for filtered results!")
                            for my_chr in my_chrs:
                                if my_chr not in chromosomes:
                                    raise WorkflowException("All batches or all variants does not have the same chromosomes for filtered results!")

            batches.append(file)
    return batches, chromosomes, variant_types


batches, chromosomes, variant_types = get_batches(cmd.wdir)
batches_new, chromosomes_new, variant_types_new = cmd.get_batches()

# Treating the case of a single dir
# TODO do this elsewhere ?
if len(batches) == 1:
    b_filtered = os.path.join(cmd.wdir, "batch001", "filtered")
    f_filtered = os.path.join(cmd.wdir, "filtered")
    if not os.path.exists(f_filtered):
        shutil.move(b_filtered, f_filtered)
        os.symlink(f_filtered, b_filtered)

localrules: bamlist, prefilter, merging, concat

rule prefilter:
    input:
        "{batch}/filtered/{svtype}/svtyper_{chrom}_{svtype}_genotypes_filtered.vcf.gz"
    output:
        "prefilter/filtered_{batch}_{chrom}_{svtype}.vcf.gz"
    params:
        reference = cmd.get_reference
    threads:
        1
    wildcard_constraints:
        batch = "batch\d+"
    shell:
        """
        bcftools view -f "PASS,." {input} | bgzip -c > {output}
        tabix -p vcf {output}
        """

rule merging:
    input:
        expand("prefilter/filtered_{batch}_{{chrom}}_{{svtype}}.vcf.gz", batch=batches)
    output:
        "merge/{svtype}/merge_{chrom}_{svtype}.vcf.gz"
    params:
        reference = cmd.get_reference
    log:
        stdout = "logs/merge/{chrom}_{svtype}.o",
        stderr = "logs/merge/{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "merge.py -o {output} -r {params.reference} -i {input} "
        "1>{log.stdout} 2>{log.stderr}"

rule bamlist:
    params:
        samples = cmd.get_samples
    output:
        bamlist = "genotypes/bamlist.list"
    run:
        with open(output.bamlist, "w") as out:
            for sample in params.samples:
                out.write("data/bams/%s.bam\n" % sample)

rule svtyping:
    input:
        sites = "merge/{svtype}/merge_{chrom}_{svtype}.vcf.gz",
        bamlist = "genotypes/bamlist.list",
        genome = cmd.get_reference
    output:
        vcf = "genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes.vcf.gz"
    params:
        outputprefix = "genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes"
    threads:
        get_threads("svtyping", 4)
    log:
        stdout = "logs/svtyper/{svtype}_{chrom}_svtyper.o",
        stderr = "logs/svtyper/{svtype}_{chrom}_svtyper.e"
    # FIXME should we remove this shadow specification ?
    # the bcftools sort in svtyper_launcher is now protected by a tmpdir
    # is sufficient or does genotype_single_sample creates probels
    shadow: "shallow"  # in order to prevent raw.vcf to be corrupted
    conda:
        "../envs/lumpysvtyperpy27.yaml"
    shell:
        "svtyper_launcher.py -B {input.bamlist} -i {input.sites} "
        "-o {output.vcf} -t {threads} 1>{log.stdout} 2>{log.stderr}"

rule annotating:
    input:
        genotypes = "genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes.vcf.gz"
    output:
        annotated = "annotated/{svtype}/svtyper_{chrom}_{svtype}_final.vcf.gz"
    params:
        genotyper = "svtyper"
    log:
        stdout = "logs/annotation/{chrom}_{svtype}.o",
        stderr = "logs/annotation/{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "annotate.py -i {input.genotypes} -o {output.annotated} "
        "-g {params.genotyper} "
        "1>{log.stdout} 2>{log.stderr}"

rule filtering:
    input:
        genotypes = "genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes.vcf.gz"
    output:
        filtered = "filtered/{svtype}/svtyper_{chrom}_{svtype}_genotypes_filtered.vcf.gz"
    params:
        genotyper = "svtyper"
    log:
        stdout = "logs/filter/{chrom}_{svtype}.o",
        stderr = "logs/filter/{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "filter.py -i {input.genotypes} -o {output.filtered} -g {params.genotyper} -k "
        "1>{log.stdout} 2>{log.stderr}"


rule concat:
    input:
         expand("annotated/{{svtype}}/svtyper_{chrom}_{{svtype}}_final.vcf.gz",
                          chrom=cmd.chromosomes)
    output:
        "results/{svtype}/cnvpipeline_{svtype}.vcf.gz"
    run:
        inputs = []
        for vcffile in input:
            if not input_is_empty(vcffile):
                inputs.append(vcffile)
        command = "bcftools concat %s | bcftools sort -Oz -o %s" %(" ".join(inputs), output[0])
        returncode = subprocess.call(command, shell=True)

rule buildresults:
    input:
        unpack(cmd.get_annotated_outputs)
    output:
        jupyter = "results/detection_summary.ipynb",
        html = "results/detection_summary.html"
    threads:
        1
    params:
        path = cmd.get_root_path
    log:
        stdout = "logs/detection_results.o",
        stderr = "logs/detection_results.e"
    shell:
        """
            if [ ! -d results ]; then
                mkdir results
            fi
            cp {params.path}/detection_summary.ipynb {output.jupyter}
            jupyter nbconvert --execute --to html {output.jupyter}
        """
