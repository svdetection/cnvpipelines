
import os
import shutil
from collections import defaultdict


def get_threads(rule, default):
    cluster_config = snakemake.workflow.cluster_config
    if rule in cluster_config and "threads" in cluster_config[rule]:
        return cluster_config[rule]["threads"]
    if "default" in cluster_config and "threads" in cluster_config["default"]:
        return cluster_config["default"]["threads"]
    return default


def get_mem(rule, default):
    cluster_config = snakemake.workflow.cluster_config
    if rule in cluster_config and "mem" in cluster_config[rule]:
        return cluster_config[rule]["mem"]
    if "default" in cluster_config and "mem" in cluster_config["default"]:
        return cluster_config["default"]["mem"]
    return default


def get_bams(sample_file):
    with open(sample_file) as fin:
        bams = [os.path.abspath(line.rstrip()) for line in fin]
    return bams


def set_sample_batches(sample_file, size_batches=10):
        samples = defaultdict()
        samples_batch = []
        start_batch_nb = 0
        with open(sample_file) as fin:
            for line in fin:
                bam = os.path.abspath(line.rstrip())
                samples_batch.append(bam)
                if len(samples_batch) == size_batches:
                    samples["batch%03d" % start_batch_nb] = samples_batch
                    samples_batch = []
                    start_batch_nb += 1
                if len(samples_batch) > 0:
                    samples["batch%03d" % start_batch_nb] = samples_batch
        return samples


configfile: "config.yaml"

bamfiles = get_bams(config['sample_file'])
reference = os.path.abspath(config['reference'])

sample_batches = set_sample_batches(config['sample_file'], size_batches=10)

workdir: config['workdir']

rootmanta = "/home/faraut/dynawork/CNVPipeline/bacAsable/manta/manta-1.6.0.centos6_x86_64"
manta_bin = rootmanta + "/bin"

shell.prefix("export PATH=%s:$PATH;" % manta_bin)

configManta = manta_bin + "/configManta.py"

localrules: regions

rule all:
    input:
        expand("{batch}/rundir/results/variants/diploidSVconverted.vcf.gz",
               batch=sample_batches.keys())

rule convert:
    input:
        vcf = "{batch}/rundir/results/variants/diploidSV.vcf.gz",
        reference = reference,
    output:
        "{batch}/rundir/results/variants/diploidSVconverted.vcf.gz"
    params:
        convertinv = rootmanta + "/libexec/convertInversion.py",
        samtoolsexe = shutil.which("samtools")
    shell:
        " {params.convertinv} {params.samtoolsexe} {input.reference} {input.vcf} "
        " | bcftools sort -Oz -o {output}"


rule configmanta:
    input:
        bams = lambda wildcards:  sample_batches[wildcards.batch],
        reference = reference,
        fai = reference+".fai",
        regions = "regions.bed.gz"
    output:
        mantarunner = "{batch}/rundir/runWorkflow.py"
    params:
        rundir = "{batch}/rundir"
    log:
        stdout = "logs/{batch}/config.o",
        stderr = "logs/{batch}/config.e"
    threads:
        1
    run:
        from subprocess import run
        command = configManta + " "
        for bam in input.bams:
            command += "--bam %s " %bam
        command += "--referenceFasta %s " % input.reference
        command += "--runDir %s " % params.rundir
        command += "--callRegions %s " % input.regions
        print(command)
        result = run(command, shell=True)

rule regions:
    input:
        reference = reference,
        fai = reference+".fai"
    output:
        "regions.bed.gz.tbi",
        region="regions.bed.gz",
    params:
        chromregex=config['chrommeregex']
    shell:
        "cat {input.fai} | egrep \'^{params.chromregex}\' "
        " | awk -v OFS='\\t' '{{ print $1,0,$2}}' | bgzip -c > {output.region}; "
        "tabix {output.region}"


rule runmanta:
    input:
        mantarunner = "{batch}/rundir/runWorkflow.py"
    output:
        temp("{batch}/rundir/workspace"),
        "{batch}/rundir/results/variants/diploidSV.vcf.gz"
    threads:
        get_threads("runmanta", 16)
    params:
        mem = get_mem("runmanta", 24)
    log:
        stdout = "logs/{batch}/run.o",
        stderr = "logs/{batch}/run.e"
    shell:
        "python2 {input} -j {threads} -g {params.mem} --quiet "
        "1>{log.stdout} 2>{log.stderr}"
