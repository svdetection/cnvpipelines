#pindel_sv = ['DEL', 'INV', 'DUP']
pindel_sv = ['DEL', 'DUP']

rule statfile:
    input:
        unpack(cmd.get_input_bams)
    output:
        statfile = "{batch}/pindel/pindel_stat.cfg"
    log:
        stdout = "{batch}/logs/pindel/statfile.o",
        stderr = "{batch}/logs/pindel/statfile.e"
    threads:
        1
    run:
        from svrunner_utils import get_insert_size
        cfg = open(output.statfile, "w")
        for sample in cmd.get_samples:
            bamfile = "data/bams/%s.bam" % sample
            statfile = "{wildcards.batch}/pindel/%s.cfg" % sample
            insert_size = get_insert_size(bamfile)
            cfg.write("\t".join([bamfile, str(insert_size), sample]) + "\n")
        cfg.close()


rule pindel:
    input:
        unpack(cmd.get_input_bams),
        statfile = "{batch}/pindel/pindel_stat.cfg",
        excluded = "exclusion/excluded.bed",
        genome = cmd.get_reference,
        fai = "%s.fai" % cmd.get_reference
    output:
        "{batch}/pindel/{chrom}/{chrbatch}/pindel_{chrom}_D.gz",
        "{batch}/pindel/{chrom}/{chrbatch}/pindel_{chrom}_INV.gz",
        "{batch}/pindel/{chrom}/{chrbatch}/pindel_{chrom}_TD.gz",
    threads:
        get_threads("pindel", 5)
    log:
        stdout = "{batch}/logs/pindel/{chrom}_{chrbatch}.o",
        stderr = "{batch}/logs/pindel/{chrom}_{chrbatch}.e"
    shell:
        # see Sudmant et al 2015 SuppInfo
        "pindel -f {input.genome} -i {input.statfile}"
        "       --min_perfect_match_around_BP 20 "
        "       --max_range_index 3 "
        "       --exclude {input.excluded}"
        "       --number_of_threads {threads}"
        "       --report_interchromosomal_events false"
        "       --window_size 0.1"  # 100kb
        "       --minimum_support_for_event 3 -c {wildcards.chrom}:{wildcards.chrbatch}"
        "       -o {wildcards.batch}/pindel/{wildcards.chrom}/{wildcards.chrbatch}/pindel_{wildcards.chrom}"
        "       1>{log.stdout} 2>{log.stderr}"
        """
        gzip -f {wildcards.batch}/pindel/{wildcards.chrom}/{wildcards.chrbatch}/*
        """

rule mergepindelbatches:
    input:
        unpack(cmd.get_pindel_chrom_batches)
    output:
        D="{batch}/pindel/pindel_{chrom}_D.gz",
        INV="{batch}/pindel/pindel_{chrom}_INV.gz",
        TD="{batch}/pindel/pindel_{chrom}_TD.gz"
    log:
        stdout = "{batch}/logs/pindel/{chrom}.o",
        stderr = "{batch}/logs/pindel/{chrom}.e",
    threads:
        1
    shell:
        "mergepindelbatches.py {output.D} {input.D} 1>{log.stdout} 2>{log.stderr};\n"
        "mergepindelbatches.py {output.INV} {input.INV} 1>>{log.stdout} 2>>{log.stderr};\n"
        "mergepindelbatches.py {output.TD} {input.TD} 1>>{log.stdout} 2>>{log.stderr};\n"
        "rm -rf {wildcards.batch}/pindel/{wildcards.chrom}"
