lumpy_sv = ['DEL', 'INV', 'DUP']

rule lumpy:
    input:
        bamlist="{batch}/bamlist.list",
        excluded="exclusion/excluded.bed"
    output:
        "{batch}/lumpy/lumpy_{chrom}.vcf.gz"
    params:
        template = cmd.get_template,
    wildcard_constraints:
        batch="\w+"
    threads:
        get_threads("lumpy", 4)
    log:
        stdout = "{batch}/logs/lumpy/{chrom}.o",
        stderr = "{batch}/logs/lumpy/{chrom}.e"
    conda:
        "../../envs/lumpysvtyperpy27.yaml"
    shell:
        "lumpy.py -b {input.bamlist} -c {wildcards.chrom}"
        " --excluded {input.excluded}"
        " -t {threads} -o {output} --template {params.template}"
        " 1>{log.stdout} 2>{log.stderr}"
