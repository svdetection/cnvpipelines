

# TODO this should appear in another place (default values crom cnvpiepelines)
config['read_length'] = 100

localrules: gendermap, bamlist

rule filtering:
    input:
        genotypes = "{batch}/genotypes/{svtype}/svtyper_{chrom}_{svtype}_genotypes.vcf.gz"
    output:
        filtered = "{batch}/filtered/{svtype}/svtyper_{chrom}_{svtype}_genotypes_filtered.vcf.gz"
    wildcard_constraints:
        batch = "\w+"
    params:
        genotyper = "svtyper"
    log:
        stdout = "{batch}/logs/filter/{chrom}_{svtype}.o",
        stderr = "{batch}/logs/filter/{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "filter.py -i {input.genotypes} -o {output.filtered} -g {params.genotyper} "
        "1>{log.stdout} 2>{log.stderr}"

rule merging:
    input:
        unpack(cmd.get_tools_output_for_svtype)
    output:
        "{batch}/merge/{svtype}/merge_{chrom}_{svtype}.vcf.gz"
    wildcard_constraints:
        batch = "\w+"
    params:
        reference = cmd.get_reference
    log:
        stdout = "{batch}/logs/merge/{chrom}_{svtype}.o",
        stderr = "{batch}/logs/merge/{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "merge.py -o {output} -r {params.reference} -i {input} "
        "1>{log.stdout} 2>{log.stderr}"

rule parsing:
    input:
        unpack(cmd.get_tool_ouput_file),
        gaps = "exclusion/gaps.bed",
        reference = cmd.get_reference,
        fai = "%s.fai" % cmd.get_reference
    output:
        parseoutput = "{batch}/parse/{svtype}/{tool}/{tool}_{chrom}_{svtype}_parsed.vcf.gz"
    wildcard_constraints:
            batch = "\w+",
            tool = "\w+"
    log:
        stdout = "{batch}/logs/{tool}/parse_{chrom}_{svtype}.o",
        stderr = "{batch}/logs/{tool}/parse_{chrom}_{svtype}.e"
    threads:
        1
    shell:
        "parse.py -r {input.reference} -i {input.tooloutput} -g {input.gaps} "
        "-o {output.parseoutput} -t {wildcards.tool} -s {wildcards.svtype} "
        "-b {wildcards.batch} "
        "1>{log.stdout} 2>{log.stderr}"

if "no_detection_index" not in config or not config["no_detection_index"]:
    rule index:
        input:
            "data/bams/{sample}.bam"
        output:
            "data/bams/{sample}.bam.bai"
        threads:
            get_threads("index", 8)
        shell:
            "samtools index -@{threads} {input}"

rule mergeexcluded:
    input:
        expand("exclusion/excluded_{sample}.bed", sample=cmd.get_samples),
    output:
        "exclusion/excluded.bed"
    log:
        stdout = "logs/exclusion/exclusion.o",
        stderr = "logs/exclusion/exclusion.e"
    shell:
        "cat {input} | bedtools sort | bedtools merge > {output}"


rule excluded:
    input:
        bam = "data/bams/{sample}.bam",
        index = "data/bams/{sample}.bam.bai",
        reference = cmd.get_reference,
        fai = "%s.fai" % cmd.get_reference
    output:
        "exclusion/excluded_{sample}.bed"
    params:
        chromosomes = ",".join(cmd.chromosomes)
    threads:
        get_threads("excluded", 4)
    log:
        stdout = "logs/exclusion/exclusion_{sample}.o",
        stderr = "logs/exclusion/exclusion_{sample}.e"
    shell:
        "detect_regions_to_exclude.py -b {input.bam}"
        " -f {input.reference}"
        " -c {params.chromosomes}"
        " -o {output}"
        " -t {threads}"
        " 1>{log.stdout} 2>{log.stderr}"

rule nstretches_d:
    input:
        reference = cmd.get_reference,
        fai = "%s.fai" % cmd.get_reference
    output:
        gaps = "exclusion/gaps.bed"
    params:
        nstrech = config['maxNstretches'] if 'maxNstretches' in config else 100
    threads:
        get_threads("excluded", 8)
    log:
        stdout = "logs/exclusion/nstretch.o",
        stderr = "logs/exclusion/nstretch.e"
    shell:
        "findNstretches.py -f {input.reference} -o {output} -t {threads} -m {params.nstrech}"
        " 1>{log.stdout} 2>{log.stderr}"

rule bamlist:
    input:
        unpack(cmd.get_input_bams)
    output:
        bamlist = "{batch}/bamlist.list"
    wildcard_constraints:
        batch = "\w+"
    run:
        cmd.dump_batch_bamlist(wildcards.batch, output.bamlist)


# rule builddetectionresults:
#     input:
#         unpack(cmd.get_filtered_outputs)
#     output:
#         jupyter = "results/detection_summary.ipynb",
#         html = "results/detection_summary.html"
#     threads:
#         1
#     params:
#         path = cmd.get_root_path
#     log:
#         stdout = "logs/detection_results.o",
#         stderr = "logs/detection_results.e"
#     shell:
#         """
#             if [ ! -d results ]; then
#                 mkdir results
#             fi
#             cp {params.path}/detection_summary.ipynb {output.jupyter}
#             jupyter nbconvert --execute --to html {output.jupyter}
#         """
