#!/usr/bin/env python

from __future__ import print_function

import os
import shutil
import traceback
import sys
import tempfile
from subprocess import call
import pysam
from pysam import VariantFile, tabix_index
from svtyper_utils import genotype_multiple_samples


def input_is_empty(input_vcf):
    is_empty = True
    vcf = pysam.VariantFile(input_vcf)
    # Check if is empty:
    for record in vcf:
        is_empty = False
        break
    return is_empty


def get_id_sample(bamfile):
    bam = pysam.AlignmentFile(bamfile)
    if "RG" not in bam.header:
        raise ValueError("Sample bam file '%s' has no RG tag in headers" % sample)
    samples_list = bam.header['RG']
    if len(samples_list) == 0:
        raise ValueError("Sample bam file '%s' empty read group (RG) list" % sample)

    # Get sample name:
    if len(samples_list) > 1:
        id_sample = None
        for rg in samples_list:
            if 'SM' not in rg:
                raise ValueError("Sample file '%s' had not SM in at least one RG tag header" % sample)
            s_sm = rg["SM"]
            if id_sample is not None and s_sm != id_sample:
                raise ValueError(
                    "Sample file '%s' has more than 1 sample (several RG tags with different SM value)"
                    % sample)
            id_sample = s_sm
    else:
        rg_headers = samples_list[0]
        if 'SM' not in rg_headers:
            raise ValueError("Sample file '%s' had not SM in RG tag header" % sample)
        id_sample = rg_headers["SM"]
    return id_sample


def get_samples(bamlist):
    samples = []
    with open(bamlist) as fh:
        for line in fh:
            id_sample = get_id_sample(line.rstrip())
            samples.append(id_sample)
    return samples


def launch(bamlist, input_vcf, output_vcf, threads):
    try:
        if input_is_empty(input_vcf):
            vcfreader = pysam.VariantFile(input_vcf)
            samples = get_samples(bamlist)
            for sample in samples:
                vcfreader.header.add_sample(sample)
            with pysam.VariantFile(output_vcf, 'w', header=vcfreader.header) as vcfwriter:
                for rec in vcfreader.fetch():
                    vcfwriter.write(rec)
            pysam.tabix_index(output_vcf, force=True, preset="vcf")
        else:
            vcf_out = output_vcf[:-3]
            genotype_multiple_samples(bamlist=bamlist,
                                      vcf_in=input_vcf,
                                      vcf_out=vcf_out,
                                      cores=threads)
            tmpdir = tempfile.mkdtemp()
            command = "bcftools sort -O z -T {tmpdir} ". format(tmpdir=tmpdir)
            command += "-o {ovcf} {ivcf} ".format(ivcf=vcf_out, ovcf=output_vcf)
            returncode = call(command, shell=True)
            if returncode == 0:
                os.remove(vcf_out)
                tabix_index(output_vcf, force=True, preset="vcf")
            else:
                print("Bctools sort command failed", file=sys.stderr)
                return 1
    except:
        # Except all exception
        traceback.print_exc()
        return 1
    return 0


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="svtyper_launcher.py",
        description="Launch svtyper on data")
    parser.add_argument("-B", "--bamlist", required=True, type=str,
                        help="File listing all input bam files")
    parser.add_argument("-i", "--input-vcf", required=True, type=str,
                        help="Input VCF file")
    parser.add_argument("-o", "--output-vcf", required=True, type=str,
                        help="Output VCF file")
    parser.add_argument("-t", "--threads", required=True, type=int,
                        help="Number of threads to use")

    args = parser.parse_args()

    exit(launch(**{k: v for k, v in vars(args).items() if k in launch.__code__.co_varnames
           or ("kwargs" in launch.__code__.co_varnames and k != "func")}))
