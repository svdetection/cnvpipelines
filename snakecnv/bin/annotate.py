#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This script annotates a vcf file"""

from svrunner_utils import eprint

from svreader.annotation import AnnotateReader, AnnotateWriter

from svreader.annotation import redundancy_annotator
from svreader.annotation import get_connected_duplicates
from svreader.annotation import variant_filtration, set_supporting_tools
from svreader.annotation import rename_variants


def filtering_by_support(variant_set, minsupp_reads=3):
    """
    Adding the LOWSUPPORT filter if the number of supporting reads is not
    least minsupp_reads for a least on sample
    """
    for variant in variant_set:
        info = variant.record.info
        if info["MAX_SUPP_READS"] < minsupp_reads:
            variant.filter.add("LOWSUPPORT")


def annotate(inputfile, outputfile, genotyper):
    """ Filtering the candidate CNVs according to the following criteria
          - non duplicate sites
          - variant sites
          - call rate > 0.8
          - at least one variant (homozygous or heterozygote) has a
            genotype quality > 20
          - the variant is not everywhere heterozygote or homozygote
            (use NONVARIANTSCORE in both cases)
          - sv with no overlap with CNVR filtered out by genomeSTRIP
    """
    # Reading the vcf file
    variant_set = []
    eprint(" Reading file %s" % (inputfile))
    reader = AnnotateReader(inputfile)

    for record in reader:
        variant_set.append(record)
    eprint("Working with " + str(len(variant_set)) + " records")

    #  Annotations
    num = 1
    for variant in variant_set:
        # PASS, and '.' become both PASS
        variant.unify_pass_filtertag()
        variant.add_supporting_infos()
        variant.set_qual()

    # Redundancy annotation
    redundancy_annotator(variant_set, reader, overlap_cutoff=0.5,
                         genotyper=genotyper)

    # Now genomeSTRIP inspired variant filtration
    if reader.numSamples() > 0:
        variant_filtration(variant_set, reader)

    # Now supporting info filtering
    filtering_by_support(variant_set)

    # Constructing connected component of duplicates
    get_connected_duplicates(variant_set)
    set_supporting_tools(variant_set)

    # Rename sv
    rename_variants(variant_set)

    vcf_fout = AnnotateWriter(outputfile, reader)
    num = 1
    for record in sorted(variant_set, key=lambda k: k.start):
        vcf_fout.write(record)
        num += 1
    vcf_fout.close()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="filter.py",
        description="Filter by applying flags to variants")
    parser.add_argument("-i", "--input-vcf", required=True,
                        help="Genotypes vcf input file")
    parser.add_argument("-o", "--output-vcf", required=True,
                        help="Filtered vcf output file")
    parser.add_argument("-g", "--genotyper", required=True,
                        help="Genotyper tool")

    args = parser.parse_args()

    annotate(inputfile=args.input_vcf,
             outputfile=args.output_vcf,
             genotyper=args.genotyper)
