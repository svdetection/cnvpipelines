#!/usr/bin/env python3

import os
import sys
import shutil
import argparse
from subprocess import run


def fail(message):
    print(message, file=sys.stderr)
    return 1


def success(message):
    print(message)
    return 0


def parse_arguments():
    parser = argparse.ArgumentParser(
        prog="genomestrip.py",
        description="Launch genomestrip on a specific chromosome"
                    "  ")
    parser.add_argument("--bamlist", help="File listing bam files", type=str, required=True)
    parser.add_argument("--interval-list", help="List of intervals to study", type=str, required=True)
    parser.add_argument("--reference", help="Reference fasta file", type=str, required=True)
    parser.add_argument("--metadata", help="Metadata folder from the preprocess job", type=str, required=True)
    parser.add_argument("--gendermap", help="Gendermap file", type=str, required=True)
    parser.add_argument("--outdir", help="Output folder", type=str, required=True)
    parser.add_argument("--outprefix", help="Output files prefix", type=str, required=True)

    return parser.parse_args()


def launch(bamlist, reference, metadata, gendermap, interval_list, outdir, outprefix):
    reference_prefix = os.path.splitext(reference)[0]
    run_dir = os.path.join(outdir, outprefix + "_run")
    sv_dir = os.environ["SV_DIR"]
    os.environ["PATH"] = sv_dir + "/bwa:" + os.environ["PATH"]
    old_libpath = ":" + os.environ["LD_LIBRARY_PATH"] if "LD_LIBRARY_PATH" in os.environ else ""
    os.environ["LD_LIBRARY_PATH"] = sv_dir + "/bwa" + old_libpath

    mx = "-Xmx8g"
    classpath = "{sv_dir}/lib/SVToolkit.jar:{sv_dir}/lib/gatk/GenomeAnalysisTK.jar:{sv_dir}/lib/gatk/Queue.jar". \
        format(sv_dir=sv_dir)

    log_dir = os.path.join(run_dir, "logs")

    if os.path.exists(log_dir):
        shutil.rmtree(log_dir)

    os.makedirs(log_dir)
    
    command = (
        """LC_ALL=C java -cp {classpath} {mx} \
org.broadinstitute.gatk.queue.QCommandLine \
-S {sv_dir}/qscript/discovery/cnv/CNVDiscoveryPipeline.q \
-S {sv_dir}/qscript/SVQScript.q \
-cp {classpath} \
-gatk {sv_dir}/lib/gatk/GenomeAnalysisTK.jar \
-configFile {sv_dir}/conf/genstrip_parameters.txt \
--disableJobReport \
-jobProject Capri \
-R {reference_prefix}.fasta \
-genderMaskBedFile  {reference_prefix}.gendermask.bed \
-genomeMaskFile     {reference_prefix}.svmask.fasta \
-genderMapFile      {gendermap} \
-ploidyMapFile      {reference_prefix}.ploidymap.txt \
-md {metadata} \
-runDirectory {run_dir} \
-jobLogDir {run_dir}/logs \
-I {bamlist} \
-intervalList {interval_list} \
-tilingWindowSize 5000 \
-tilingWindowOverlap 2500 \
-maximumReferenceGapLength 2500 \
-boundaryPrecision 200 \
-minimumRefinedLength 2500 \
-run""".format(classpath=classpath, mx=mx, sv_dir=sv_dir, reference_prefix=reference_prefix, gendermap=gendermap,
               run_dir=run_dir, interval_list=interval_list, bamlist=bamlist, metadata=metadata))

    with_drmaa = "SV_PARALLEL_DRMAA" in os.environ and os.environ["SV_PARALLEL_DRMAA"] == "1"

    if with_drmaa:
        command += " -jobRunner Drmaa \
    -gatkJobRunner Drmaa \
    -jobQueue workq \
    -jobNative \"${SV_PARALLEL_NATIVES}\" "

    exit_code = run(command, shell=True).returncode

    if exit_code != 0:
        return fail("Run of cnvpipelines (step 1/2) failed")

    sites = os.path.join(run_dir, "results", "gs_cnv.genotypes.vcf.gz")
    genotypes = os.path.join(outdir, outprefix + ".vcf.gz")

    # Run genotyping on the discovered sites.
    command = (
        """LC_ALL=C  java -Xmx4g -cp {classpath} {mx} \
 org.broadinstitute.sv.apps.GenerateHaploidCNVGenotypes \
 -R {reference_prefix}.fasta \
 -ploidyMapFile {reference_prefix}.ploidymap.txt \
 -genderMapFile {gendermap}  \
 -vcf {sites} \
 -O {genotypes} \
 -estimateAlleleFrequencies true \
 -genotypeLikelihoodThreshold 0.001""".format(classpath=classpath, mx=mx, reference_prefix=reference_prefix,
                                              gendermap=gendermap, sites=sites, genotypes=genotypes))

    exit_code = run(command, shell=True).returncode

    if exit_code != 0:
        return fail("Run of cnvpipelines (step 2/2) failed")

    return success("Completed successfully")


def main():
    args = parse_arguments()

    # Check files
    bamlist = args.bamlist
    if not os.path.isfile(bamlist):
        return fail("File %s does not exists or is not a file" % bamlist)
    reference = args.reference
    if not os.path.isfile(reference):
        return fail("File %s does not exists or is not a file" % reference)
    gendermap = args.gendermap
    if not os.path.isfile(gendermap):
        return fail("File %s does not exists or is not a file" % gendermap)
    metadata = args.metadata
    if not os.path.isdir(metadata):
        return fail("Folder %s does not exists or is not a folder" % metadata)

    # Launch
    return launch(**{k: v for k, v in vars(args).items() if k in launch.__code__.co_varnames})


if __name__ == "__main__":
    sys.exit(main())
