#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Usage:
    merge.py  -o output.vcf.gz -r reference.fasta -i input.vcf.gz [input2.vcf.gz ...]

"""

import os
import tempfile
import shutil
import subprocess

from svrunner_utils import addContigInfosTovcf


def run_wrapper(cmd):
    try:
        subprocess.run(cmd, check=True, shell=True)
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)


def concatenate(vcf_files, outputfile, reference):
    """
        Remove INFO, FORMAT and FILTER from the header
    """
    tempdir = tempfile.mkdtemp()
    vcfFileslist = tempdir + "/" + "vcffile.list"
    with open(vcfFileslist, "w") as outfh:
        for gfile in vcf_files:
            vcf_sample_dropped_file = tempdir + "/" + os.path.basename(gfile)
            cmd = "bcftools view --drop-genotypes " + gfile + \
                  " |  bcftools sort -Oz -o " + vcf_sample_dropped_file
            cmd += " && tabix -p vcf " + vcf_sample_dropped_file
            run_wrapper(cmd)
            outfh.write(vcf_sample_dropped_file + "\n")
    temp_raw_gz = tempdir + "/" + "merge_raw.vcf.gz"
    cmd = "bcftools concat -f " + vcfFileslist + \
          " -a | bcftools sort -Oz -o " + temp_raw_gz
    run_wrapper(cmd)

    # Remove all info fields and format fields
    temp_annotate_gz = tempdir + "/" + "merge_annotate.vcf.gz"
    remove_exp = "FORMAT,FILTER"
    remove_exp += ",^INFO/SVTYPE,^INFO/SVLEN,^INFO/CIPOS,^INFO/END,^INFO/CIEND"
    remove_exp += ",^INFO/TOOLSUPPORT"
    cmd = "bcftools annotate -x %s -Oz -o %s %s " % (remove_exp,
                                                     temp_annotate_gz,
                                                     temp_raw_gz)
    run_wrapper(cmd)

    # Correct contig infos in vcf
    addContigInfosTovcf(temp_annotate_gz, outputfile, reference)

    # Correct contig infos here
    shutil.rmtree(tempdir)


def merging(inputfiles, outputfile, reference):

    concatenate(inputfiles, outputfile, reference)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="merge.py",
        description="Merge VCF files into a single one")
    parser.add_argument("-i", "--input-vcfs", required=True,
                        help="Vcf input files", nargs='+')
    parser.add_argument("-o", "--output-vcf", required=True,
                        help="Merged vcf output file")
    parser.add_argument("-r", "--reference", required=True,
                        help="Reference file")

    args = parser.parse_args()

    merging(inputfiles=args.input_vcfs,
            outputfile=args.output_vcf,
            reference=args.reference)
