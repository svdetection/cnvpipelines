#! /usr/bin/env python3

import sys
import argparse
import re

import multiprocessing
from functools import partial

from pysam import FastaFile


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_chromosomes(fastafile):
    with FastaFile(fastafile) as fasta:
        chromosomes = fasta.references
    return chromosomes


def find_chrom_Nstretches(chrom, fastafile, cutoff):
    p = re.compile("N{%s,}" % cutoff)
    with FastaFile(fastafile) as fasta:
        sequence = fasta.fetch(chrom)
        gaps = [(chrom, m.start(), m.end()) for m in re.finditer(p, sequence)]
    return gaps


def findNstretches(infile, minsize, outfile, cores):
    chromosomes = get_chromosomes(infile)
    pool = multiprocessing.Pool(processes=cores)
    launch_chrom = partial(find_chrom_Nstretches,
                           fastafile=infile,
                           cutoff=minsize)

    results = pool.map(launch_chrom, chromosomes)
    genome_gaps = [item for sublist in results for item in sublist]
    dumpgaps(genome_gaps, outfile)

def dumpgaps(gaps, outputfile):
    with open(outputfile, "w") as fout:
        for chrom, start, end in gaps:
            fout.write("%s\t%d\t%d\tN\t%d\n" % (chrom, start, end, end - start))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Construct a bed file "
                                     "describing the stretches of strictly "
                                     "greater than minsize N ")
    parser.add_argument('-f', '--file', required=True, dest="infile",
                        help='name fo the fasta file')
    parser.add_argument('-o', '--output', required=True,
                        help='name fo the fasta file')
    parser.add_argument('-m', '--minsize', type=int, default=100,
                        help='minimum required stretch size of N')
    parser.add_argument('-t', '--threads', type=int, default=1,
                        help='number of threads')

    args = parser.parse_args()

    findNstretches(args.infile, args.minsize, args.output, args.threads)
