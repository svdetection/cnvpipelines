

include: "common.smk"

cmd = CommandFactory.factory(config)

workdir: config['wdir']

rule all:
    input:
        cmd.get_all_outputs

include: "rules/detectioncommon.smk"
include: "rules/tools/delly.smk"
include: "rules/tools/lumpy.smk"
include: "rules/tools/pindel.smk"
include: "rules/tools/genomestrip.smk"
include: "rules/tools/genotyping.smk"
include: "rules/tools/cnvpipeline.smk"
