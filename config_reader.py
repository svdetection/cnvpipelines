import os
import sys
import inspect
from pathlib import Path
from configparser import RawConfigParser, NoOptionError, NoSectionError


class Singleton:
    def __init__(self, klass):
        self.klass = klass
        self.instance = None

    def __call__(self, *args, **kwds):
        if self.instance is None:
            self.instance = self.klass(*args, **kwds)
        return self.instance


class MissingOption(Exception):
    pass


@Singleton
class AppConfigReader:
    """
    Store all configs
    """

    def __init__(self):
        """
        All "get_*" functions results are stored in the "self.*" corresponding attribute
        Example: results of the get_upload_folder function is stored in self.upload_folder
        """
        self.app_dir = os.path.dirname(inspect.getfile(self.__class__))
        config_file = []
        config_file_search = [os.path.join(self.app_dir, "application.properties"),
                              os.path.join(str(Path.home()), ".cnvpipelines", "application.properties")]

        for my_config_file in config_file_search:
            if os.path.exists(my_config_file):
                config_file.append(my_config_file)

        if len(config_file) == 0:
            raise FileNotFoundError("ERROR: application.properties not found.")
        self.reader = RawConfigParser()
        self.reader.read(config_file)
        for attr in dir(self):
            attr_o = getattr(self, attr)
            if attr.startswith("_get_") and callable(attr_o):
                setattr(self, attr[5:], attr_o())

    def _replace_vars(self, path):
        return path.replace("###USER###", os.path.expanduser("~"))\
            .replace("###PROGRAM###", self.app_dir)\
            .replace("###SYSEXEC###", os.path.dirname(sys.executable))

    def _get_batch_system_type(self):
        try:
            return self.reader.get("global", "batch_system_type")
        except (NoSectionError, NoOptionError):
            return "local"

    def _get_modules(self):
        try:
            return self.reader.get("global", "modules")
        except (NoSectionError, NoOptionError):
            return ""

    def _get_paths(self):
        try:
            return self.reader.get("global", "paths")
        except (NoSectionError, NoOptionError):
            return ""

    def _get_n_jobs(self):
        try:
            return int(self.reader.get("global", "jobs"))
        except (NoSectionError, NoOptionError):
            return 999

    def _get_cluster_submission_mode(self):
        try:
            return self.reader.get("cluster", "submission_mode")
        except (NoSectionError, NoOptionError):
            # TODO should this be local instead of cluster ?
            return "cluster"

    def _get_sv_dir(self):
        # If genomestrip conda command is available
        # we use the corresponding dir as sv_dir
        ## TODO  make it possible to overwrite sv_dir in conf
        conda_svdir = genomestrip_dir()
        if os.path.exists(conda_svdir):
            return conda_svdir
        try:
            return self.reader.get("global", "sv_dir")
        except (NoSectionError, NoOptionError):
            return ""


    def _get_cluster_submission_command(self):
        try:
            command = self.reader.get("cluster", "submission_command")
            if command == "" and self._get_batch_system_type() != "local" and \
                    self._get_cluster_submission_mode() == "cluster":
                raise MissingOption("Application.properties: With cluster submission mode, "
                                    "submission_command in required")
            return command
        except (NoSectionError, NoOptionError):
            if self._get_batch_system_type() != "local" and  self._get_cluster_submission_mode() == "cluster":
                raise MissingOption("Application.properties: With cluster submission mode, "
                                    "submission_command in required")
            return None

    def _get_cluster_drmaa_lib(self):
        try:
            lib = self.reader.get("cluster", "drmaa")
            if lib == "" and self._get_batch_system_type() != "local" and \
                    self._get_cluster_submission_mode() == "drmaa":
                raise MissingOption("Application.properties: please specify the DRMAA lib path or use the cluster "
                                    "submission mode")
            return lib
        except (NoSectionError, NoOptionError):
            if self._get_batch_system_type() != "local" and self._get_cluster_submission_mode() == "drmaa":
                raise MissingOption("Application.properties: please specify the DRMAA lib path or use the cluster "
                                 "submission mode")
            return None

    def _get_cluster_native_submission_options(self):
        try:
            natives = self.reader.get("cluster", "native_submission_options")
        except (NoSectionError, NoOptionError):
            natives = "###DEFAULT###"
        if natives == "###DEFAULT###":
            batch_type = self._get_batch_system_type()
            if batch_type == "slurm":
                return (" --mem-per-cpu={cluster.mem}000 --mincpus={threads} --time={cluster.time} -J {cluster.name}"
                        " -N 1")
            elif batch_type == "sge":
                return " -l mem={cluster.mem}G -l h_vmem={cluster.mem}G -pe parallel_smp {threads} -N {cluster.name}"
            return None
        return natives

    def _get_cluster_config_file(self):
        try:
            config = self._replace_vars(self.reader.get("cluster", "config"))
            if config == "" and self._get_batch_system_type() != "local":
                raise MissingOption("Application.properties: please specify a cluster config file or use a"
                                    "local batch system type")
            return config
        except (NoSectionError, NoOptionError):
            if self._get_batch_system_type() != "local":
                raise MissingOption("Application.properties: please specify a cluster config file or use a"
                                    "local batch system type")
            return None

    def _get_repeatmasker_lib_path(self):
        try:
            return self._replace_vars(self.reader.get("refbundle", "repeatmasker_lib_path"))
        except (NoSectionError, NoOptionError):
            return ""

    def _get_sv_parallel_drmaa(self):
        if self._get_batch_system_type() == "local":
            return False
        try:
            return self.reader.get("cluster", "sv_parallel_drmaa").lower() == "true"
        except (NoSectionError, NoOptionError):
            # TODO default should be false ?
            return True

    def _get_sv_parallel_native_specs(self):
        try:
            natives = self.reader.get("cluster", "sv_parallel_native_specs")
            if natives == "###DEFAULT###":
                batch_type = self._get_batch_system_type()
                if batch_type == "slurm":
                    return "--mem-per-cpu=32000 -t 24:00:00"
                elif batch_type == "sge":
                    return "-l mem=32G -l h_vmem=32G"
                return None
        except (NoSectionError, NoOptionError):
            batch_type = self._get_batch_system_type()
            if batch_type == "slurm":
                return "--mem-per-cpu=32000 -t 24:00:00"
            elif batch_type == "sge":
                return "-l mem=32G -l h_vmem=32G"
            return None


def which(pgm):
    path = os.getenv('PATH')
    for p in path.split(os.path.pathsep):
        p = os.path.join(p, pgm)
        if os.path.exists(p) and os.access(p, os.X_OK):
            return p


def genomestrip_dir():
    "Get SV_DIR from the genomestrip exectable"
    "resolve source until the file is no longer a symlink"
    source = which("genomestrip")
    if source is None:
        return ""
    while os.path.islink(source):
        pointsto = os.readlink(source)
        sourcedir = os.path.dirname(source)
        source = os.path.abspath(os.path.join(sourcedir, pointsto))
    return os.path.dirname(source)
