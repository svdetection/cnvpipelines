#!/usr/bin/env python3

from config_reader import AppConfigReader, MissingOption
from glob import glob
import os
import pysam
import re
import shutil
import yaml
import requests
import hashlib
import traceback
import math
import time
import sys
from datetime import datetime
from pathlib import Path
from subprocess import run

TOOLS = ["lumpy", "delly", "pindel", "genomestrip"]
VTYPES = ["DEL", "INV", "DUP", "mCNV"]
MIN_CHR_SIZE = 500000

MD5_TEST = {
    "ERR470101_f_1.fq.gz": "2c30600e303c9bfb7b9538ec76055203",
    "ERR470101_f_2.fq.gz": "0fb2391381be350d04485dfecd02c4e6",
    "ERR470102_f_1.fq.gz": "1195e5e03c2be1cc8aa1af4002fff05b",
    "ERR470102_f_2.fq.gz": "ffd8b8596e9f5c43f60ad9a687f7ae7c",
    "ERR470103_f_1.fq.gz": "51f5e0a63a3f379aa94a27551423e32d",
    "ERR470103_f_2.fq.gz": "4def2c968d3522f4adc3ea4d083f582c",
    "ERR470105_f_1.fq.gz": "9e638c619e486e1da9ce4cb011b1f0b4",
    "ERR470105_f_2.fq.gz": "b5dd22c09403f45cc718671f6569a5f4",
    "CHIR_1.0.sel_subsample.fa": "14e21345bbaa2742e7a16696dc526a13",
    "CHIR_1.0.sel_subsample.fa.fai": "1129ecadcde2587e0066ac97dfe4b280"
}


class FileCorrupted(Exception):
    pass


class CnvPipeline:

    def __init__(self, wdir, debug=False):
        self.wdir = os.path.abspath(wdir)
        self.debug = debug
        self.delete_wdir_after_dryrun = False
        if not os.path.exists(self.wdir):
            os.makedirs(self.wdir)
            self.delete_wdir_after_dryrun = True
        self.config_file = os.path.join(self.wdir, "config.yaml")
        self.app_dir = os.path.dirname(os.path.realpath(__file__))
        self.config = AppConfigReader()
        self.wf_type_file = os.path.join(self.wdir, ".wf")
        self._wf_type = "unknown"
        self._load_wf_type()
        self._filename_for_url = {}
        self.isrerun = False
        self.config_timestamp = None
        self.config_data = None

    @property
    def wf_type(self):
        if self._wf_type == "unknown":
            self._load_wf_type()
        return self._wf_type

    def _save_wf_type(self, wf_type):
        with open(self.wf_type_file, "w") as wf_type_file:
            wf_type_file.write(wf_type)
        self._wf_type = wf_type

    def _load_wf_type(self):
        if os.path.exists(self.wf_type_file):
            with open(self.wf_type_file, "r") as wf_type_file:
                self._wf_type = wf_type_file.read().rstrip()
        else:
            self._wf_type = "unknown"

    @staticmethod
    def md5(fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def _write_samples(self, samples, sample_file):
        with open(sample_file, "w") as samples_fh:
            samples_fh.write("\n".join(samples) + "\n")
        # self._write_samples_bam_tsv(samples, sample_file)

    def _write_samples_bam_tsv(self, samples, sample_file):
        # write in addition a yaml file with sample names and bams
        # we have therefore two files : sample list and sample tsv
        # (with bam file path)
        samples_tsv = os.path.splitext(sample_file)[0] + ".tsv"
        with open(samples_tsv, "w") as samples_bam:
            samples_bam.write("sample\tbam\n")
            for sample in samples:
                bam = "data/bams/" + sample + ".bam"
                samples_bam.write("%s\t%s\n" % (sample, bam))

    def _link_bams_wdir(self, samples_file, bams_dir, smple_file, nb_samples):
        """
        Link each BAM file in the data dir and write the new sample file
        :param samples_file: file listing full path of all bam files, given as input by the user
        :type samples_file: str
        :param bams_dir: folder which will contain final bam files (as links)
        :type bams_dir: str
        :param smple_file: final sample files listing all samples, by name (without path)
        :type smple_file: str
        :param nb_samples: number of samples to add
        :type nb_samples: int
        :param n_block: number of the current block
        :type n_block: int
        """
        with open(samples_file, "r") as samples:
            samples_name = []
            all_samples_name = []
            all_bam_files = []
            # Ignore previous lines:
            added_samples = 0
            n_block = 1
            # Add current block:
            for sample in samples:
                sample = sample.rstrip()
                if sample == "":
                    continue
                sample = os.path.abspath(sample)

                # Some checks:
                if not os.path.isfile(sample):
                    raise ValueError("Sample bam file '%s' does not exists" % sample)
                bam = pysam.AlignmentFile(sample)
                if "RG" not in bam.header:
                    raise ValueError("Sample bam file '%s' has no RG tag in headers" % sample)
                samples_list = bam.header['RG']
                if len(samples_list) == 0:
                    raise ValueError("Sample bam file '%s' empty read group (RG) list" % sample)

                # Get sample name:
                if len(samples_list) > 1:
                    id_sample = None
                    for rg in samples_list:
                        if 'SM' not in rg:
                            raise ValueError("Sample file '%s' had not SM in at least one RG tag header" % sample)
                        s_sm = rg["SM"]
                        if id_sample is not None and s_sm != id_sample:
                            raise ValueError(
                                "Sample file '%s' has more than 1 sample (several RG tags with different SM value)"
                                % sample)
                        id_sample = s_sm
                else:
                    rg_headers = samples_list[0]
                    if 'SM' not in rg_headers:
                        raise ValueError("Sample file '%s' had not SM in RG tag header" % sample)
                    id_sample = rg_headers["SM"]

                # Link bam file:
                final_file = os.path.join(bams_dir, id_sample + ".bam")
                if not os.path.exists(final_file):
                    os.symlink(sample, final_file)

                # Link index file if already exists
                bai_final = final_file + ".bai"
                bai_orig = sample + ".bai"
                if os.path.isfile(bai_orig):
                    if not os.path.exists(bai_final):
                        os.symlink(bai_orig, bai_final)
                else:
                    bai_orig = sample.rsplit(".", 1)[0] + ".bai"
                    if os.path.isfile(bai_orig) and not os.path.exists(bai_final):
                        os.symlink(bai_orig, bai_final)

                added_samples += 1
                samples_name.append(id_sample)
                all_samples_name.append(id_sample)
                if added_samples == nb_samples:
                    # write the block samples name
                    self._write_samples(samples_name, smple_file + "." + str(n_block))
                    samples_name = []
                    added_samples = 0
                    n_block += 1

            if len(samples_name) > 0:  # Sample names for the batch
                self._write_samples(samples_name, smple_file + "." + str(n_block))

            if len(all_samples_name) > 0:  # All samples names
                self._write_samples(all_samples_name, smple_file)



    @staticmethod
    def _fail(message):
        """
        Print fail message and exit
        :param message: error message
        """
        #cprint("WARNING: " + message + ", exiting softly!", 'magenta',
        #   attrs=['bold'], file=sys.stderr)
        print("\033[31m\033[1mAn error has occurred:\n%s\033[0m" % message)
        exit(1)

    def _get_wdir_type(self):
        with open(os.path.join(self.wdir, ".wf"), "r") as f:
            wf_type = f.read().rstrip()
        return wf_type

    def _check_wdir(self, force=False):
        """
        Check working dir at run: search for an existing workflow
        """
        if os.path.isdir(self.wdir):
            if os.path.exists(self.config_file):
                accepted_values = ["y", "n", ""]
                confirm = None
                while confirm not in accepted_values:
                    if self.wf_type != self._get_wdir_type():
                        print("Error: working directory already exists and is not a " + self.wf_type +
                              " job. Exiting...", file=sys.stderr)
                        exit(1)
                    if confirm is not None:
                        print("Invalid choice!")
                    else:
                        message = "Working dir %s already exists." % self.wdir
                        if self.wf_type == "detection":
                            message += " We will use previous samples and genome and ignore given ones."
                        elif self.wf_type == "simulation":
                            print("Error: working directory already exists. "
                                  "Not allowed for simulation jobs. Exiting...", file=sys.stderr)
                            exit(1)
                        print(message)
                    if force:
                        print("WARN: Continue without confirmation (forced mode)")
                        confirm = "y"
                    else:
                        confirm = input("Continue? [y/N] ").lower()
                if confirm == "n" or confirm == "":
                    print("Run canceled.")
                    exit(0)
                self.isrerun = True
        else:
            if os.path.exists(self.wdir):
                raise ValueError("Output dir exists but is not a folder")
            os.makedirs(self.wdir)

        if self.isrerun:
            self.config_timestamp = datetime.fromtimestamp(os.stat(self.config_file).st_ctime).\
                strftime("%Y-%m-%d %H:%M:%S")
            with open(self.config_file, "r") as cfg:
                self.config_data = yaml.safe_load(cfg)

    def _prerun_commands(self):
        """
        Set commands to launch before running snakemake
        :return: list of commands
        :rtype list
        """
        commands = []
        if len(self.config.modules) > 0:
            commands.append("module load %s" % self.config.modules)
        if len(self.config.paths) > 0:
            commands.append("export PATH=%s:$PATH" % self.config.paths)
        commands.append("export SV_DIR=\"%s\"" % self.config.sv_dir)

        if self.wf_type in ["refbundle", "simulation"] and self.config.repeatmasker_lib_path != "":
            commands.append("export REPEATMASKER_LIB_DIR=%s" % self.config.repeatmasker_lib_path)

        commands.append("export SV_PARALLEL_DRMAA=%d" % (1 if self.config.sv_parallel_drmaa else 0))
        if self.config.sv_parallel_native_specs is not None:
            commands.append("export SV_PARALLEL_NATIVES=\"%s\"" % self.config.sv_parallel_native_specs)

        return commands

    def _get_snakefile(self):
        try:
            if self.wf_type == "detection":
                snakefile = "detection.smk"
            elif self.wf_type == "refbundle":
                snakefile = "referencebundle.smk"
            elif self.wf_type == "align":
                snakefile = "align.smk"
            elif self.wf_type == "mergebatches":
                snakefile = "mergebatches.smk"
            elif self.wf_type == "simulation":
                snakefile = "simulation.smk"
            elif self.wf_type == "test":
                snakefile = "test.snk"
            elif self.wf_type == "unknown":
                raise ValueError("Type of workflow not set")
            else:
                raise ValueError("Type of workflow not supported: %s" % self.wf_type)
        except ValueError as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))
        else:
            return os.path.join(self.app_dir, "snakecnv", snakefile)

    @staticmethod
    def _find_fasta_file(folder):
        """
        Find reference fasta file in a folder
        :param folder: folder into search
        :return: full path of the reference file (None if not found)
        """
        reference = None
        for file in glob(os.path.join(folder, "*.fasta")):
            if reference is None or len(file) < len(reference):
                reference = file
        if reference is None:
            for file in glob(os.path.join(folder, "*.fa")):
                if reference == "" or len(file) < len(reference):
                    reference = file
        return os.path.abspath(reference) if reference is not None else None

    def __get_filename_from_url(self, url):
        if url not in self._filename_for_url:
            if url.startswith("ftp://"):
                self._filename_for_url[url] = url.split("/")[-1]
            elif url.startswith("http://") or url.startswith("https://"):
                self._filename_for_url[url] = requests.head(url, allow_redirects=True).url.split("/")[-1]
            else:
                return None
        return self._filename_for_url[url]

    def _download_file(self, url, outdir):
        basename = self.__get_filename_from_url(url)
        local_filename = os.path.join(outdir, basename)
        if os.path.exists(local_filename) and basename in MD5_TEST and self.md5(local_filename) == MD5_TEST[basename]:
            return local_filename, False
        print("Downloading %s..." % basename, flush=True)
        # NOTE the stream=True parameter
        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush() commented by recommendation from J.F.Sebastian
        return local_filename, True

    def _check_file(self, local_file):
        local_file_basename = os.path.basename(local_file)
        print("Checking %s..." % local_file_basename, flush=True)
        try:
            if local_file_basename not in MD5_TEST:
                raise KeyError("file %s not present in MD5_TEST variable!" % local_file_basename)
            if self.md5(local_file) != MD5_TEST[local_file_basename]:
                raise FileCorrupted("MD5 check fails for file %s" % local_file_basename)
        except (KeyError, FileCorrupted) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def _download_test_files(self, ok):
        """
        Download files for test run
        :param ok: file to touch if succeed
        :return: 0 if succeed
        """
        print("Downloading files...", flush=True)
        fastq_dir = os.path.join(self.wdir, "fastq")
        if not os.path.exists(fastq_dir):
            os.makedirs(fastq_dir)
        for fastq in ["http://ng6.toulouse.inra.fr/fileadmin/data/run/dc1ef9fb8/ERR470101_f_1.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/dc1ef9fb8/ERR470101_f_2.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/01253581f/ERR470105_f_1.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/01253581f/ERR470105_f_2.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/fd3691164/ERR470103_f_1.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/fd3691164/ERR470103_f_2.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/1f628bfa6/ERR470102_f_1.fq.gz",
                      "http://ng6.toulouse.inra.fr/fileadmin/data/run/1f628bfa6/ERR470102_f_2.fq.gz"]:
            local_file, check = self._download_file(fastq, fastq_dir)
            if check:
                self._check_file(local_file)
        reference_dir = os.path.join(self.wdir, "reference")
        if not os.path.exists(reference_dir):
            os.makedirs(reference_dir)
        for file in ["http://ng6.toulouse.inra.fr/fileadmin/data/analyze/024a58590/CHIR_1.0.sel_subsample.fa",
                     "http://ng6.toulouse.inra.fr/fileadmin/data/analyze/024a58590/CHIR_1.0.sel_subsample.fa.fai"]:
            local_file, check = self._download_file(file, reference_dir)
            if check:
                self._check_file(local_file)

        Path(ok).touch()
        return 0

    def unlock_job(self):
        """
        Unlock a job
        """
        try:
            if not os.path.exists(self.config_file):
                raise FileNotFoundError("File not found: %s" % self.config_file)
            if not os.path.isfile(self.config_file):
                raise FileNotFoundError("File exists but is not a file: %s" % self.config_file)
        except FileNotFoundError as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))
        commands = self._prerun_commands()
        commands.append("snakemake -s {snakefile} --unlock --configfile {config_file}".format(
            snakefile=self._get_snakefile(), config_file=self.config_file
        ))

        command = "\n".join(commands)
        print(command)
        exit_code = os.system(command)
        if exit_code == 0:
            print("Job is now unlocked")

        return 0

    def run_or_rerun(self, dryrun=False, printshellcmds=False, clean=False, soft_clean=False, cluster_config=None,
                     out_step=None, rerun_incomplete=False, force_unlock=False, merge_batches=False,
                     keep_wdir=False, prerun_commands=True, latency_wait=5, snake_args=dict(), mode="run", reason=False,
                     *args, **kwargs):
        """
        Run  or rerun a workflow (all data must be already ready)
        :param dryrun: True to only show the workflow as it will be launched without launch it
        :type dryrun: bool
        :param printshellcmds: True to show launched commands by each rule of the workflow
        :type printshellcmds: bool
        :param clean: True to clean intermediate data
        :type clean: bool
        :param cluster_config: custom cluster config file (erase the default one)
        :type cluster_config: str
        :param out_step: out step until go for snakemake run
        :type out_step: str
        :param rerun_incomplete: if True, rerun incomplete rules
        :type rerun_incomplete: bool
        :param force_unlock: if True, unlock the job before rerun it
        :type force_unlock: bool
        :param merge_batches: if True, rerun the merge batches workflow
        :type merge_batches: bool
        :param reason: if True, show reason of each executed rule
        :type reason: bool
        """

        if self.isrerun:
            run(["touch", self.config_file, "-d", self.config_timestamp])
        if mode == "rerun" and self.wf_type == "detection" and len(snake_args) == 0:
                local_vars = locals()
                del local_vars["self"]
                del local_vars["snake_args"]
                return self.run_or_rerun_detection(**local_vars)
            # Fix snakemake bug: make reference files timestamp to the config.yaml timestamp value
#        if (mode == "rerun" or self.isrerun) and (self.wf_type == "simulation" or self.wf_type == "refbundle"):
#                #reference = os.path.join(self.wdir, "reference.fasta")
#                reference = os.path.join(self.wdir, "reference_raw.fasta")
#                fixed_date = datetime.fromtimestamp(os.stat(self.config_file).st_ctime).strftime("%Y-%m-%d %H:%M:%S")
#                if os.path.exists(reference):
#                    run(["touch", "-h", reference, "-d", fixed_date])
#                fai = reference + ".fai"
#                if os.path.exists(fai):
#                    run(["touch", "-h", fai, "-d", fixed_date])
#
#                if self.wf_type == "simulation":
#                    reference = os.path.join(self.wdir, "refbundle", "reference.fasta")
#                    if os.path.exists(reference):
#                        run(["touch", reference, "-d", fixed_date])
#                    fai = reference + ".fai"
#                    if os.path.exists(fai):
#                        run(["touch", fai, "-d", fixed_date])

        if merge_batches:
            if self.wf_type != "detection":
                self._fail("Merge batches can only be done on a detection workflow")
            self._wf_type = "mergebatches"



        try:
            if not os.path.exists(self.config_file):
                raise FileNotFoundError("File not found: %s" % self.config_file)
            if not os.path.isfile(self.config_file):
                raise FileNotFoundError("File exists but is not a file: %s" % self.config_file)
        except FileNotFoundError as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

        if force_unlock:
            self.unlock_job()

        # Prepare command to launch:
        commands = []
        if prerun_commands:
            commands = self._prerun_commands()

        # Snakemake command itself:
        snakefile = self._get_snakefile()
        snk_command = "snakemake --jobs {jobs} -s {snakefile} --use-conda".format(
            jobs=self.config.n_jobs, snakefile=snakefile
        )

        if self.config.batch_system_type != "local":
            if self.config.cluster_submission_mode == "drmaa":
                commands.append("export DRMAA_LIBRARY_PATH=\"%s\"" % self.config.cluster_drmaa_lib)
                cluster_options = "--drmaa \""
            else:
                cluster_options = "--cluster \"" + self.config.cluster_submission_command + " "
            cluster_options += self.config.cluster_native_submission_options + "\""

            snk_command += " --cluster-config {clusterconf} {clusteroptions}".format(
                clusterconf=self.config.cluster_config_file if cluster_config is None else cluster_config,
                clusteroptions=cluster_options
            )
            if os.path.isfile(self.config.cluster_drmaa_lib):
                commands.append(
                    "export LD_LIBRARY_PATH=%s:$LD_LIBRARY_PATH" % os.path.dirname(self.config.cluster_drmaa_lib))

        snk_command += " --configfile {config_file}".format(config_file=self.config_file)

        if dryrun:
            snk_command += " -n"
        if printshellcmds:
            snk_command += " -p"
        if rerun_incomplete:
            snk_command += " --rerun-incomplete"
        if reason:
            snk_command += " -r"

        if out_step is not None:
            snk_command += " " + out_step

        if len(snake_args) > 0:
            snk_command += " --config " + " ".join(["=".join(map(str, x)) for x in snake_args.items()])

        snk_command += " --latency-wait %d" % latency_wait

        commands.append("cd %s" % self.wdir)

        commands.append(snk_command)
        command = "; ".join(commands)

        # Launch command:
        print("Launching snakemake workflow command:")
        print(command, flush=True)
        exit_code = run(command, shell=True).returncode
        if exit_code == 0:
            if clean:
                self.clean()
            elif soft_clean:
                self.soft_clean()
        if dryrun and self.wf_type != "detection" and self.delete_wdir_after_dryrun and not keep_wdir:
            shutil.rmtree(self.wdir)
        return exit_code

    def run_or_rerun_detection(self, **kwargs):

        sample_files = glob(os.path.join(self.wdir, "samples.list.*"))
        blocks = [int(x.rsplit(".", 1)[1]) for x in sample_files]
        blocks.sort()

        start_batch_nb = 1

        with open(self.config_file, "r") as cfg_file:
            cfg = yaml.load(cfg_file, Loader=yaml.FullLoader)

        batches_parallel = cfg["batches"]

        exit_status = 1

        do_delete_after_dryrun = self.delete_wdir_after_dryrun

        for n_block in blocks:
            sample_file = os.path.join(self.wdir, "samples.list.%d" % n_block)

            with open(sample_file, "r") as smple_file:
                samples_name = [x.rstrip() for x in smple_file if x.rstrip() != ""]

            block_done = os.path.join(self.wdir, ".done%d" % n_block)
            if not os.path.exists(block_done):
                print("Launching block of samples number %d..." % n_block, flush=True)
                print("Samples:", flush=True)
                print("\t-" + "\n\t-".join(samples_name), flush=True)
                print("", flush=True)

                # if size_pbatches > 0 and n_block > 1:
                #     start_batch_nb = ((n_block - 1) * batches_parallel) + 1
                # Run:
                self.delete_wdir_after_dryrun = False
                exit_status = self.run_or_rerun(snake_args={"start_batch_nb": start_batch_nb,
                                                            "sample_file": sample_file},
                                                 **kwargs)

                if exit_status == 0 and (not "dryrun" in kwargs or not kwargs["dryrun"]):
                    Path(block_done).touch()
                elif exit_status != 0:
                    return exit_status

            start_batch_nb += math.ceil(len(samples_name) / batches_parallel)

        if ("dryrun" in kwargs and kwargs["dryrun"]) and (not "keep_wdir" in kwargs or not kwargs["keep_wdir"])\
                and ("mode" not in kwargs or kwargs["mode"] == "run") and do_delete_after_dryrun:
            shutil.rmtree(self.wdir)

        return exit_status

    def _chromosomes_filtered(self, fai_file, valid_chr):
        chromosomes = []
        regex = re.compile("(^" + "$)|(^".join(valid_chr) + "$)")
        with open(fai_file, "r") as fai:
            for line in fai:
                cols = line.rstrip().split("\t")
                chr_name = cols[0]
                if re.match(regex, chr_name):
                    chromosomes.append(chr_name)
        if len(chromosomes) == 0:
            self._fail("No valid chromosomes found in your reference. May be the list of chromosomes you set is not"
                       "correct?")
        print("List of chromosomes included in this analysis:")
        for chrm in chromosomes:
            print("\t- %s" % chrm)
        print("")
        time.sleep(1)
        return chromosomes

    def _chromosomes_list(self, fai_file, dofilter=False):
        chromosomes = []
        filtered = False
        with open(fai_file, "r") as fai:
            for line in fai:
                cols = line.rstrip().split("\t")
                chr_name = cols[0]
                chr_len = int(cols[1])
                if not dofilter or chr_len >= MIN_CHR_SIZE:
                    chromosomes.append(chr_name)
                else:
                    filtered = True
        if filtered:
            print("Warning: you enable the chromosomes filtering. We will not work on all chromosomes.")
            print("List of chromosomes included in this analysis:")
            for chrm in chromosomes:
                print("\t- %s" % chrm)
            print("")
            time.sleep(1)
        if len(chromosomes) == 0:
            self._fail("No valid chromosomes found in your reference. Are all your chromosomes smaller than 500 kb?")
        return chromosomes

    def _rerun_tools(self, new_tools):
        """
        Remove post-parsed steps on rerun detection if tools changed

        :param new_tools:
        :return:
        """
        old_tools = self.config_data["tools"]
        tools_changed = False
        if len(old_tools) == len(new_tools):
            for tool in new_tools:
                if tool not in old_tools:
                    tools_changed = True
                    break
        else:
            tools_changed = True

        if tools_changed:
            for folder in glob(os.path.join(self.wdir, "batch*"), recursive=False):
                for fold in ("merge", "genotypes", "filtered"):
                    t_folder = os.path.join(folder, fold)
                    if os.path.exists(t_folder):
                        if not os.path.isdir(t_folder):
                            raise ValueError(t_folder + "exists but is not a folder")
                        shutil.rmtree(t_folder)

    def run_detection(self, reference, tools, samples, variant_types, chromosomes="all", force_wdir=False, batches=-1,
                      batches_parallel=5, refbundle=None, force_all_chr=False, **kwargs):

        #print("rundetection",self, reference, tools, samples, variant_types, chromosomes="all", force_wdir=False, batches=-1,batches_parallel=5, refbundle=None, force_all_chr=False, **kwargs)
        """
        Run a detection workflow
        :param reference: reference genome file
        :type reference: str
        :param tools: list of tools
        :type tools: list
        :param samples: file containing all samples
        :type samples: str
        :param chromosomes: list of chromosomes, coma separated, or "all"
        :type chromosomes: str
        :param force_all_chr: if True, add all chromosomes without filtering by size
        :type force_all_chr: bool
        :param force_wdir: force erase of wdir
        :type force_wdir: bool
        :param batches: size of batches (-1 to make only one batch)
        :type batches: int
        :param batches_parallel: number of batches to launch in parallel (-1 to all, else >= 1)
        :type batches_parallel: int
        :param refbundle: reference bundle folder
        :type refbundle: str
        """
        try:
            # Check arguments:
            if not os.path.isfile(reference):
                raise ValueError("Reference file does not exists")
            if type(tools) != list and type(tools) != tuple:
                raise TypeError("Tools must be a list")
            for tool in tools:
                if tool not in tools:
                    raise ValueError("Tool not available: %s" % tool)
            if not os.path.isfile(samples):
                raise ValueError("Sample file does not exists")
            if not isinstance(batches, int):
                raise ValueError("batches must be an integer")
            if not isinstance(batches_parallel, int):
                raise ValueError("batches_parallel must be an integer")
            if batches_parallel != -1 and batches_parallel < 1:
                raise ValueError("batches parallel must be equal to -1 or up to 1")
            self._check_wdir(force_wdir)

            # Check variant types:
            final_variant_types = []
            for variant_type in variant_types:
                if variant_type not in VTYPES:
                    raise ValueError("Disallowed variant type: %s" % variant_type)
                if variant_type not in final_variant_types:
                    final_variant_types.append(variant_type)

            # Get absolute paths
            reference = os.path.abspath(reference)

            final_reference = os.path.join(self.wdir, "reference.fasta")
            final_fai = final_reference + ".fai"

            if not self.isrerun:
                if not os.path.exists(final_reference):
                    os.symlink(reference, final_reference)
                    fai = reference + ".fai"
                    if os.path.isfile(fai):
                        os.symlink(fai, final_fai)
                    else:
                        print("Indexing reference fasta file...")
                        run_status = run("samtools faidx %s" % final_reference, shell=True)
                        if run_status.returncode != 0:
                            self._fail("Indexing reference fasta file failed")
                            return False
                elif not os.path.exists(final_fai):
                    self._fail("Working directory is corrupted: reference.fasta.fai not found in the existing working "
                               "directory")
                    return False
            else:
                self._rerun_tools(new_tools=tools)
                # Remove done files
                done_files = glob(os.path.join(self.wdir, ".done*"))
                for done_f in done_files:
                    os.remove(done_f)
                if self.config_data["batches"] != batches:
                    print("Error: rerun with a different number of batch size not allowed. Exiting...", file=sys.stderr)
                    exit(1)

            if chromosomes == "all":
                chromosomes = self._chromosomes_list(final_fai, not force_all_chr)
            else:
                chromosomes = self._chromosomes_filtered(final_fai, chromosomes)

            if refbundle is not None:
                refbundle = self._find_fasta_file(refbundle)
                if refbundle is None:
                    raise ValueError("refbundle folder is invalid: it does not contains a reference fasta file")
            elif "mCNV" in variant_types:
                final_variant_types.remove("mCNV")

            size_pbatches = -1 if batches == -1 else batches * batches_parallel

            config = {
                "wf_type": "detection",
                "wdir": self.wdir,
                "reference": "reference.fasta",
                "tools": tools,
                "batches": batches,
                "parallel_batches": batches_parallel,
                "chromosomes": chromosomes,
                "varianttypes": final_variant_types,
            }

            if refbundle is not None:
                config["refbundle"] = refbundle

            with open(self.config_file, "w") as config_file:
                yaml.dump(config, config_file, default_flow_style=False)

            self._save_wf_type("detection")

            if not self.isrerun:
                # Prepare data:
                bams_dir = os.path.join(self.wdir, "data", "bams")
                if not os.path.exists(bams_dir):
                    os.makedirs(bams_dir)
                final_sample_file = os.path.join(self.wdir, "samples.list")
                self._link_bams_wdir(samples_file=samples, bams_dir=bams_dir,
                                     smple_file=final_sample_file, nb_samples=size_pbatches)

            return self.run_or_rerun_detection(**kwargs)

        except (ValueError, TypeError, MissingOption) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def run_refbundle(self, reference, species, read_length=100, max_n_stretches=100, force_wdir=False,
                      chromosomes="all", force_all_chr=False, **kwargs):
        """
        Run a refbundle workflow
        :param reference: reference genome file
        :type reference: str
        :param species: species name, according to the NCBI Taxonomy database
        (http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html)
        :type species: str
        :param read_length: read length
        :type read_length: int
        :param max_n_stretches: maximum n stretches length
        :type max_n_stretches: int
        :param force_wdir: force erase of wdir
        :type force_wdir: bool
        :param chromosomes: list of chromosomes, coma separated, or "all"
        :type chromosomes: str
        :param force_all_chr: if True, add all chromosomes without filtering by size
        :type force_all_chr: bool
        """
        try:
            if not os.path.isfile(reference):
                raise ValueError("Reference file does not exists")
            self._check_wdir(force_wdir)

            # Get absolute paths
            reference = os.path.abspath(reference)

            final_reference = os.path.join(self.wdir, "reference_raw.fasta")
            final_fai = final_reference + ".fai"

            if not self.isrerun:
                if not os.path.exists(final_reference):
                    os.symlink(reference, final_reference)
                    fai = reference + ".fai"
                    if os.path.isfile(fai):
                        os.symlink(fai, final_fai)
                    else:
                        print("Indexing reference fasta file...")
                        exit_code = run("samtools faidx %s" % final_reference, shell=True).returncode
                        if exit_code != 0:
                            self._fail("Indexing reference fasta file failed")
                            return False
                elif not os.path.exists(final_fai):
                    self._fail("Working directory is corrupted: reference.fasta.fai not found in the existing working "
                               "directory")
                    return False

            if chromosomes == "all":
                chromosomes = self._chromosomes_list(final_fai, not force_all_chr)
            else:
                chromosomes = self._chromosomes_filtered(final_fai, chromosomes)

            config = {
                "wf_type": "refbundle",
                "reference": "reference_raw.fasta",
                "wdir": self.wdir,
                "species": species,
                "read_len": read_length,
                "maxNstretches": max_n_stretches,
                "chromosomes": chromosomes
            }

            with open(self.config_file, "w") as config_file:
                yaml.dump(config, config_file, default_flow_style=False)

            self._save_wf_type("refbundle")

            # Run:
            return self.run_or_rerun(**kwargs)

        except (ValueError, TypeError, MissingOption) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def parse_align_sample_file(self, input_sample_file, valid_sample_file):
        with open(input_sample_file, "r") as old_s, open(valid_sample_file, "w") as new_s:
            valid_samples = {}
            try:
                y_samples = yaml.load(old_s, Loader=yaml.FullLoader)
            except yaml.YAMLError:
                if self.debug:
                    traceback.print_exc()
                raise ValueError("Invalid yaml file: %s" % samples)
            else:
                if not isinstance(y_samples, dict):
                    raise ValueError("Error: sample file is not valid")
                for sample, files in y_samples.items():
                    valid_samples[sample] = {}
                    if not isinstance(files, dict):
                        raise ValueError("Error: sample file is not valid. File line must be: `reads1: /path/to/file`. "
                                        "May be you forgot a space?")
                    for name, file in files.items():
                        if name not in ["reads1", "reads2"]:
                            raise ValueError("Invalid sample \"%s\" in sample file: %s" % (sample, samples))
                        elif name in valid_samples[sample]:
                            raise ValueError("Invalid sample \"%s\" in sample file: %s" % (sample, samples))
                        if not os.path.exists(file) or not os.path.isfile(file):
                            raise ValueError("Sample \"%s\": file \"%s\" does not exists or is not a file" %
                                             (sample, file))
                        valid_samples[sample][name] = os.path.abspath(file)
                yaml.dump(valid_samples, new_s, default_flow_style=False)
                # We dump here also a tsv file
                # FIXME keep a single sample file in work dir (the tsv file)
                # FIXME remove the so called valid_sample_file yaml file
                #self._dump_tsv_sample_file(valid_samples, valid_sample_file)

    def _dump_tsv_sample_file(self, valid_samples, valid_sample_file):
        samples_tsv = os.path.splitext(valid_sample_file)[0] + ".tsv"
        with open(samples_tsv, "w") as samples_fh:
            samples_fh.write("sample\treads\n")
            for sample in valid_samples:
                sample_fastqs = self._get_fastq_path(valid_samples[sample])
                samples_fh.write("%s\t%s\n" % (sample, ",".join(sample_fastqs)))

    def _get_fastq_path(self, sample_fastq):
        if len(sample_fastq) == 1:
            return [sample_fastq["reads"]]
        else:
            return [sample_fastq["reads1"], sample_fastq["reads2"]]

    def run_align(self, reference, samples, force_wdir=False, **kwargs):
        """
        Run an align workflow (fastq on reference to bam file)
        :param reference: reference genome file
        :type reference: str
        :param samples: yaml sample file
        :type samples: str
        :param force_wdir: force erase of wdir
        :type force_wdir: bool
        """
        try:
            if not os.path.isfile(reference):
                raise ValueError("Reference file does not exists")
            self._check_wdir(force_wdir)

            # Get absolute paths
            reference = os.path.abspath(reference)

            ref_dir = os.path.join(self.wdir, "reference")
            if not os.path.exists(ref_dir):
                os.makedirs(ref_dir)
            elif not os.path.isdir(ref_dir):
                raise FileExistsError(ref_dir + " already exists but is not a folder")

            if not self.isrerun:
                for file in glob(reference + "*"):
                    final_link = os.path.join(ref_dir, os.path.basename(file))
                    if os.path.exists(final_link):
                        if os.path.islink(final_link):
                            os.remove(final_link)
                        else:
                            raise FileExistsError(file + "already exists but is not a link")
                    os.symlink(file, final_link)

            final_sample_file = os.path.join(self.wdir, "samples.yml")
            self.parse_align_sample_file(samples, final_sample_file)

            config = {
                "wf_type": "align",
                "wdir": self.wdir,
                "reference": os.path.join(ref_dir, os.path.basename(reference)),
                "sample_file": final_sample_file
            }

            with open(self.config_file, "w") as config_file:
                yaml.dump(config, config_file, default_flow_style=False)

            self._save_wf_type("align")

            # Run:
            return self.run_or_rerun(**kwargs)

        except (ValueError, TypeError, MissingOption, FileExistsError) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def run_mergebatches(self, **kwargs):
        """
        Run mergebatches workflow
        :param kwargs:
        :return:
        """
        sample_files = glob(os.path.join(self.wdir, "samples.list.*"))
        sample_files.sort(key=lambda x: int(x.rsplit(".", 1)[1]))
        sample_file_final = os.path.join(self.wdir, "samples.list")
        with open(sample_file_final, "w") as smple_file_f:
            for sample_file in sample_files:
                with open(sample_file, "r") as smple_f:
                    text = smple_f.read()
                    if not text.endswith("\n"):
                        text += "\n"
                    smple_file_f.write(text)

        with open(self.config_file, "r") as cfg_file:
            conf = yaml.load(cfg_file, Loader=yaml.FullLoader)

        conf["wf_type"] = "mergebatches"
        conf["sample_file"] = sample_file_final
        with open(self.config_file, "w") as cfg_file:
            yaml.dump(conf, cfg_file, default_flow_style=False)

        return self.run_or_rerun(merge_batches=True, **kwargs)

    @staticmethod
    def _link(source, dest):
        """
        Link file if dest file does not exists
        :param source:
        :param dest:
        """
        if not os.path.exists(dest):
            os.symlink(source, dest)

    @staticmethod
    def _create_samples_fastq_simulaton(nb_inds, smple_file):
        """
        Create samples file for align workflow of simulation pipeline
        :param nb_inds: number of individuals generated for simulation
        :param smple_file: path of the sample file
        """
        samples = {}
        for i in range(1, nb_inds+1):
            samples["indiv%d" % i] = {
                "reads1": os.path.join("pop", "indiv%d_1.fq.gz" % i),
                "reads2": os.path.join("pop", "indiv%d_2.fq.gz" % i)
            }
        with open(smple_file, "w") as samples_f:
            yaml.dump(samples, samples_f, default_flow_style=False)

    @staticmethod
    def _create_samples_detection_simulation(nb_inds, smple_file):
        """
        Create samples file for detection workflow of simulation pipeline
        :param nb_inds: number of individuals generated for simulation
        :param smple_file: path of the sample file
        """
        with open(smple_file, "w") as samples_f:
            for i in range(1, nb_inds + 1):
                samples_f.write("indiv%d\n" % i)

    def _check_sv_list(self, svlist_file, required_variants):
        """
        Check all variants types are described in the variants list file

        :param svlist_file: variants list file describing variants
        :type svlist_file: str
        :param required_variants: list of variants that must be present on the sv_list file
        :type required_variants: list
        :return: True if OK
        """
        variants = set()
        with open(svlist_file, "r") as svs:
            for line in svs:
                variants.add(re.split(r"\s+", line.rstrip())[0])

        valid = True
        messages = []
        for req_variant in required_variants:
            if req_variant not in variants:
                valid = False
                messages.append("%s variant has a probability higher than 0 but is not described in the variants file"
                                % req_variant)
        if not valid:
            message = "\n".join(messages) + "\nPlease add them to the file or set the probability to 0 for them."
            self._fail(message)

        return valid

    def run_simulation(self, nb_inds, reference, sv_list, coverage, force_polymorphism,
                       haploid, proba_del, proba_inv, proba_dup, read_len, insert_len_mean, insert_len_sd, min_deletions,
                       min_inversions, min_duplications, freq_del, freq_inv, freq_dup, max_try, genotypes, tools, species, max_n_stretches,
                       overlap_cutoff, left_precision, right_precision, chromosomes, force_all_chr=False,
                       force_wdir=False, **kwargs):
        """
        Run simulation
        :param nb_inds:
        :param reference:
        :param sv_list:
        :param coverage:
        :param force_polymorphism:
        :param haploid:
        :param proba_del:
        :param proba_inv:
        :param read_len:
        :param insert_len_mean:
        :param insert_len_sd:
        :param min_deletions:
        :param min_inversions:
        :param max_try:
        :param genotypes:
        :param tools:
        :param kwargs:
        :param species:
        :param max_n_stretches:
        :param force_wdir:
        :return:
        """
        try:
            if not os.path.isfile(reference):
                raise ValueError("Reference file does not exists")
            reference = os.path.abspath(reference)
            if sv_list is not None:
                if not os.path.isfile(sv_list):
                    raise ValueError("Sv_list file does not exists")
                sv_list = os.path.abspath(sv_list)
            if genotypes is not None:
                if not os.path.isfile(genotypes):
                    raise ValueError("Genotypes file does not exists")
                genotypes = os.path.abspath(genotypes)
            if proba_del < 0:
                raise ValueError("proba_del must be positive or null")
            if proba_inv < 0:
                raise ValueError("proba_inv must be positive or null")
            if proba_dup < 0:
                raise ValueError("proba_dup must be positive or null")
            if proba_inv == 0 and proba_del == 0 and proba_dup == 0:
                raise ValueError("At least one proba of variant (del/inv/dup) must be positive")
            if "genomestrip" in tools and species is None:
                raise ValueError("Species parameter is required for genomestrip")
            if proba_inv == 0:
                min_inversions = 0
            elif proba_del == 0:
                min_deletions = 0
            elif proba_dup == 0:
                min_duplications = 0

            self._check_wdir(force_wdir)

            final_reference = os.path.join(self.wdir, "reference_raw.fasta")
            final_fai = final_reference + ".fai"

            if not os.path.exists(final_reference):
                os.symlink(reference, final_reference)
                fai = reference + ".fai"
                if os.path.isfile(fai):
                    os.symlink(fai, final_fai)
                else:
                    print("Indexing reference fasta file...")
                    exit_code = run("samtools faidx %s" % final_reference, shell=True).returncode
                    if exit_code != 0:
                        self._fail("Indexing reference fasta file failed")
                        return False
            elif not os.path.exists(final_fai):
                self._fail("Working directory is corrupted: reference_raw.fasta.fai not found in the existing working "
                           "directory")
                return False


            refbundle_dir = os.path.join(self.wdir, "refbundle")
            refbundle_fasta = os.path.join(refbundle_dir, "reference_raw.fasta")

            if "genomestrip" in tools:
                if not os.path.exists(refbundle_fasta):
                    if not os.path.exists(refbundle_dir):
                        os.makedirs(refbundle_dir)
                    os.symlink(final_reference, refbundle_fasta)
                    refbundle_fai = refbundle_fasta + ".fai"
                    if not os.path.exists(refbundle_fai):
                        os.symlink(final_fai, refbundle_fai)

            if chromosomes == "all":
                chromosomes = self._chromosomes_list(final_fai, not force_all_chr)
            else:
                chromosomes = self._chromosomes_filtered(final_fai, chromosomes)

            # Create sample file for fastq files:
            samples_file_fq = os.path.join(self.wdir, "samples.yml")
            samples_file_d = os.path.join(self.wdir, "samples.list")

            self._create_samples_fastq_simulaton(nb_inds, samples_file_fq)
            self._create_samples_detection_simulation(nb_inds, samples_file_d)

            variant_types = []
            if proba_del > 0:
                variant_types.append("DEL")
            if proba_inv > 0:
                variant_types.append("INV")
            if proba_dup > 0:
                variant_types.append("DUP")
            config = {
                "wf_type": "simulation",
                "wdir": self.wdir,
                "nb_inds": nb_inds,
                "reference": "reference_raw.fasta",
                "coverage": coverage,
                "force_polymorphism": force_polymorphism,
                "haploid": haploid,
                "proba_del": proba_del,
                "proba_inv": proba_inv,
                "proba_dup": proba_dup,
                "read_len": read_len,
                "insert_len_mean": insert_len_mean,
                "insert_len_sd": insert_len_sd,
                "min_deletions": min_deletions,
                "min_inversions": min_inversions,
                "min_duplications": min_duplications,
                "freq_del": freq_del,
                "freq_inv": freq_inv,
                "freq_dup": freq_dup,
                "max_try": max_try,
                "sample_file_align": samples_file_fq,  # NOT used
                "tools": tools,
                "sample_file": samples_file_d,   # NOT used
                "batches": -1,
                "parallel_batches": -1,
                "start_batch_nb": 1,
                "chromosomes": chromosomes,
                "varianttypes": variant_types,
                "no_detection_index": True,
                "build_refbundle": "genomestrip" in tools,
                "species": species,
                "maxNstretches": max_n_stretches,
                "overlap_cutoff": overlap_cutoff,
                "left_precision": left_precision,
                "right_precision": right_precision,
                "rb_subdir": "refbundle"
            }

            if "genomestrip" in tools:
                config["refbundle"] = os.path.join("refbundle", "reference.fasta")
            if sv_list is not None:
                svlist_final = os.path.join(self.wdir, os.path.basename(sv_list))
                self._link(sv_list, svlist_final)
                config["sv_list"] = svlist_final
                self._check_sv_list(svlist_final, variant_types)
            if genotypes is not None:
                genotypes_final = os.path.join(self.wdir, os.path.basename(genotypes))
                self._link(genotypes, genotypes_final)
                config["genotypes"] = genotypes_final

            with open(self.config_file, "w") as cfg_file:
                yaml.dump(config, cfg_file, default_flow_style=False)

            self._save_wf_type("simulation")

            # Run:
            return self.run_or_rerun(**kwargs)

        except (ValueError, TypeError, MissingOption, FileExistsError) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def run_test(self, **kwargs):
        try:
            self._check_wdir()
            download_ok = os.path.join(self.wdir, ".downloads-ok")
            if not os.path.exists(download_ok):
                self._download_test_files(download_ok)
            print("Copy config files...")
            shutil.copy(os.path.join(self.app_dir, "samples_test.yaml"), os.path.join(self.wdir, "samples.yaml"))
            config = {
                "cnvpipeline": os.path.join(self.app_dir, "cnvpipelines.py"),
                "wdir": self.wdir
            }
            with open(self.config_file, "w") as config_file:
                yaml.dump(config, config_file, default_flow_style=False)

            with open(os.path.join(self.wdir, "samples_bams.list"), "w") as bam_smple:
                for sample in ["ERR470101", "ERR470102", "ERR470103", "ERR470105"]:
                    bam_smple.write(os.path.join(self.wdir, "align", "bams", "%s.bam\n" % sample))

            self._save_wf_type("test")

            # Run:
            return self.run_or_rerun(prerun_commands=False, **kwargs)

        except (ValueError, TypeError, MissingOption, FileExistsError) as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))

    def _clean(self, keep_files, fake):
        for file in os.listdir(self.wdir):
            abs_file_path = os.path.join(self.wdir, file)
            if abs_file_path not in keep_files and file != "config.yaml":
                if os.path.isdir(abs_file_path):
                    if fake:
                        print(abs_file_path)
                    else:
                        shutil.rmtree(abs_file_path)
                else:
                    if fake:
                        print(abs_file_path)
                    else:
                        os.remove(abs_file_path)

    def _clean_detection(self, fake):
        keep_files = [os.path.join(self.wdir, x) for x in ["filtered", "data", "samples.list"]]
        self._clean(keep_files, fake)

    def _clean_refbundle(self, fake):
        keep_files = glob(os.path.join(self.wdir, "reference*[!.log]"))
        self._clean(keep_files, fake)

    def clean(self, fake=False):
        """
        Clean working dir
        :param fake: if True, only show deleted files without removing them
        """
        try:
            if not os.path.exists(self.wdir):
                raise ValueError("Working dir does not exists: %s" % self.wdir)
            if not os.path.isdir(self.wdir):
                raise ValueError("Working dir is not a folder: %s" % self.wdir)
            print("Cleaning...")
            if self.wf_type == "detection":
                self._clean_detection(fake)
            elif self.wf_type == "refbundle":
                self._clean_refbundle(fake)
            elif self.wf_type == "unknown":
                raise ValueError("Type of workflow not set")
            else:
                raise ValueError("Type of workflow not supported: %s" % self.wf_type)
            print("Cleaned!")
            return 0

        except ValueError as e:
            if self.debug:
                traceback.print_exc()
            self._fail(str(e))


    def soft_clean(self, fake=False):
        """
        Clean only log files
        :param fake: if True, only show deleted files without removing them
        """
        print("Cleaning...")
        logs = glob(os.path.join(self.wdir, "**", "logs"), recursive=True)
        for log in logs:
            if os.path.isdir(log):
                if fake:
                    print(log)
                else:
                    shutil.rmtree(log)
        cluster_logs = re.compile(r"(^slurm-\d+\.out$)"
                                  r"|(^.+\.[eo]\d+$)"
                                  r"|(^.+\.sh\.\d+\.[eo]$)")
        for file in os.listdir(self.wdir):
            abs_file_path = os.path.join(self.wdir, file)
            if re.match(cluster_logs, file) and os.path.isfile(abs_file_path):
                if fake:
                    print(abs_file_path)
                else:
                    os.remove(abs_file_path)
        print("Cleaned!")
        return 0


def _launch(args):
    """
    Launch function associated to argparse subparser
    :param args: argparse arguments object
    :type args: argparse.Namespace
    """
    cnv = CnvPipeline(**{k: v for k, v in vars(args).items() if k in CnvPipeline.__init__.__code__.co_varnames})
    func = cnv.__getattribute__(args.func)
    exit_code = func(**{k: v for k, v in vars(args).items() if k in func.__code__.co_varnames
                or ("kwargs" in func.__code__.co_varnames and k != "func")})
    if not isinstance(exit_code, int) or exit_code > 255:
        return 1
    return exit_code


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Run cnv pipelines")
    subparsers = parser.add_subparsers(metavar="command")
    subparsers.required = True

    def check_min_size_0(value):
        ivalue = int(value)
        if ivalue < 0:
            raise argparse.ArgumentTypeError("%s is an invalid size value (>= 0)" % value)
        return ivalue

    def add_run_and_rerun_options(subparser, mergebatches=False, clean=True):
        subparser.add_argument('-n', '--dryrun', action="store_true", required=False, default=False,
                               help="Only dry run snakemake")
        subparser.add_argument('-p', '--printshellcmds', action="store_true", required=False,
                               default=False, help="Print shell commands")
        if clean:
            subparser.add_argument('-c', '--clean', action="store_true", required=False, default=False,
                                   help="Clean data after run")
            subparser.add_argument('-sc', '--soft-clean', action="store_true", required=False,
                                   default=False, help="Clean logs after run")
        if not mergebatches:
            subparser.add_argument('-f', '--force-wdir', action="store_true", required=False,
                                   default=False, help="Force run, erase working dir automatically")
        subparser.add_argument('--cluster-config', type=str, required=False, default=None,
                               help="Path to a cluster config file (erase the cluster config present in configution")
        subparser.add_argument('--out-step', type=str, required=False, default=None,
                               help="Output file of the step you want the output (if not specified, run all workflow)")
        subparser.add_argument('--latency-wait', type=int, required=False, default=5,
                               help=("Wait given seconds if an output file of a job is not "
                                     "present after the job finished. This helps if your "
                                     "filesystem suffers from latency (default 5)"))
        subparser.add_argument('--reason', action="store_true", required=False, default=False,
                               help="Show reason of each executed rule")
        subparser.add_argument('--debug', action="store_true", required=False, default=False, help="Enable debug")



    def add_run_options(subparser):
        subparser.add_argument('--keep-wdir', action="store_true", required=False, default=False,
                               help="On dry run keep working directory")


    # Run command:
    run_parser = subparsers.add_parser("run", help="Run a workflow")
    run_parsers = run_parser.add_subparsers()

    # ---Detection:
    detection_parser = run_parsers.add_parser("detection", help="CNV detection workflow")
    detection_parser.add_argument('-r', '--reference', type=str, required=True, help="Reference fasta file")
    detection_parser.add_argument('-rb', '--refbundle', type=str, required=False, help="Reference bundle folder")
    detection_parser.add_argument('-t', '--tools', type=str, required=True, help="Tools to launch, coma separated",
                                  nargs="+", choices=TOOLS)
    detection_parser.add_argument('-s', '--samples', type=str, required=True, help="File containing all samples")
    add_run_and_rerun_options(detection_parser)
    detection_parser.add_argument('-b', '--batches', type=int, required=False, default=-1,
                                  help="Size of batches, -1 to unlimited size [default: -1]")
    detection_parser.add_argument('-bp', '--batches-parallel', type=int, required=False, default=5,
                                  help="Number of batch to launch in parallel, -1 to launch all batch in parallel "
                                       "[default: 5]")
    detection_parser.add_argument('--chromosomes', type=str, required=False, default="all",
                                  help="List of chromosomes to study, space separated [default: all]", nargs='+')
    detection_parser.add_argument('--force-all-chromosomes', action="store_true", required=False, default=False,
                                  dest="force_all_chr", help="Do not remove small contigs (< 500bk) from the analysis")
    detection_parser.add_argument('-w', '--wdir', type=str, required=True,
                           help="Output folder where data will be stored")
    detection_parser.add_argument('-v', '--variant-types', type=str,
                                  help="type of variants to detect, space separated (default: DEL, INV and DUP)",
                                  choices=VTYPES, nargs="+", default=["DEL", "INV", "DUP"])
    add_run_options(detection_parser)
    detection_parser.set_defaults(func="run_detection")

    # ---Reference bundle:
    refbundle_parser = run_parsers.add_parser("refbundle", help="Build reference bundle for genomestrip")
    refbundle_parser.add_argument('-r', '--reference', type=str, required=True, help="Reference fasta file")
    refbundle_parser.add_argument('-s', '--species', type=str, required=True,
                                  help="Species name, according to the NCBI Taxonomy database")
    refbundle_parser.add_argument('-l', '--read-length', type=int, required=False, default=100,
                                  help="Read length")
    refbundle_parser.add_argument('-m', '--max-n-stretches', type=int, required=False, default=100)
    add_run_and_rerun_options(refbundle_parser)
    refbundle_parser.add_argument('--chromosomes', type=str, required=False, default="all",
                                  help="List of chromosomes to study, space separated [default: all]", nargs='+')
    refbundle_parser.add_argument('--force-all-chromosomes', action="store_true", required=False, default=False,
                                  dest="force_all_chr", help="Do not remove small contigs (< 500bk) from the analysis")
    refbundle_parser.add_argument('-w', '--wdir', type=str, required=True,
                                  help="Output folder where data will be stored")
    add_run_options(refbundle_parser)
    refbundle_parser.set_defaults(func="run_refbundle")

    # ---Align:
    align_parser = run_parsers.add_parser("align", help="Align fastq files on a reference genome into bam files")
    align_parser.add_argument('-r', '--reference', type=str, required=True, help="Reference fasta file")
    align_parser.add_argument('-s', '--samples', type=str, required=True, help="Yaml file containing all samples "
                                                                               "with associated fastq files")
    add_run_and_rerun_options(align_parser)
    align_parser.add_argument('-w', '--wdir', type=str, required=True,
                              help="Output folder where data will be stored")
    add_run_options(align_parser)
    align_parser.set_defaults(func="run_align")

    # ---Merge batches:
    mergebatches_parser = run_parsers.add_parser("mergebatches", help="Merge batches of a previous detection run")
    mergebatches_parser.add_argument('-w', '--wdir', type=str, required=True,
                                  help="Output folder where detection data are stored")
    add_run_and_rerun_options(mergebatches_parser, True)
    add_run_options(mergebatches_parser)
    mergebatches_parser.set_defaults(func="run_mergebatches")

    # ---Simulation
    simul_parser = run_parsers.add_parser("simulation", help="Run a complete simulation workflow")
    simul_parser.add_argument("-nb", "--nb-inds", help="Number of individuals to generate", type=int, required=True)
    simul_parser.add_argument("-r", "--reference", help="Reference genome", required=True)
    simul_parser.add_argument("-s", "--sv-list", help="File containing the SVs", required=False)
    simul_parser.add_argument("-cv", "--coverage", help="Coverage of reads", default=15, type=int)
    simul_parser.add_argument("-fp", "--force-polymorphism", help="Force polymorphism for each SV",
                              action='store_const', const=True, default=False)
    simul_parser.add_argument("-a", "--haploid", help="Make a haploid genome, instead of diploid one",
                              action="store_const",
                              const=True, default=False)
    simul_parser.add_argument("-pd", "--proba-del", help="Probabilty to have a deletion", type=float, default=0.000001)
    simul_parser.add_argument("-pi", "--proba-inv", help="Probablity to have an insertion", type=float,
                              default=0.000001)
    simul_parser.add_argument("-pu", "--proba-dup", help="Probablity to have a duplication", type=float,
                              default=0.000001)
    simul_parser.add_argument("-l", "--read-len", help="Generate reads having a length of LEN", type=int, default=100)
    simul_parser.add_argument("-m", "--insert-len-mean",
                              help="Generate inserts (fragments) having an average length of LEN",
                              type=int, default=300)
    simul_parser.add_argument("-v", "--insert-len-sd",
                              help="Set the standard deviation of the insert (fragment) length (%%)",
                              type=int, default=30)
    simul_parser.add_argument("-md", "--min-deletions", help="Minimum of deletions to generate (>=0)", default=1,
                              type=check_min_size_0)
    simul_parser.add_argument("-mi", "--min-inversions", help="Minimum of inversions to generate (>=0)", default=1,
                              type=check_min_size_0)
    simul_parser.add_argument("-mu", "--min-duplications", help="Minimum of duplications to generate (>=0)", default=1,
                              type=check_min_size_0)
    simul_parser.add_argument('-fd', '--freq-del', type=float, required=False,
                              help="Frequencies choices for deletions, space separated", nargs="+", default=[0.2, 0.5])
    simul_parser.add_argument('-fi', '--freq-inv', type=float, required=False,
                              help="Frequencies choices for inversions, space separated", nargs="+", default=[0.2, 0.5])
    simul_parser.add_argument('-fu', '--freq-dup', type=float, required=False,
                              help="Frequencies choices for duplications, space separated", nargs="+", default=[0.2, 0.5])
    simul_parser.add_argument("--max-try", help="Maximum of tries", default=10, type=check_min_size_0)
    simul_parser.add_argument("-g", "--genotypes", help="Position of SV with genotypes of individuals")
    simul_parser.add_argument('-t', '--tools', type=str, required=True, help="Tools to launch, space separated",
                              nargs="+", choices=TOOLS)
    simul_parser.add_argument('-sp', '--species', type=str, required=False,
                              help="[refbundle] Species name, according to the NCBI Taxonomy database",
                              default="human")
    simul_parser.add_argument('-mn', '--max-n-stretches', type=int, required=False, default=100,
                                  help="[refbundle] Max size of nstretches to consider them as it")
    simul_parser.add_argument('--overlap-cutoff', type=float, default=0.5,
                              help='[results] cutoff for reciprocal overlap')
    simul_parser.add_argument('--left-precision', type=int, default=-1, help='[results] left breakpoint precision')
    simul_parser.add_argument('--right-precision', type=int, default=-1, help='[results] right breakpoint precision')
    add_run_and_rerun_options(simul_parser)
    simul_parser.add_argument('--chromosomes', type=str, required=False, default="all",
                                  help="List of chromosomes to study, space separated [default: all]", nargs='+')
    simul_parser.add_argument('--force-all-chromosomes', action="store_true", required=False, default=False,
                                  dest="force_all_chr", help="Do not remove small contigs (< 500bk) from the analysis")
    simul_parser.add_argument('-w', '--wdir', type=str, required=True,
                              help="Output folder where data will be stored")
    add_run_options(simul_parser)
    simul_parser.set_defaults(func="run_simulation")

    # Rerun command:
    rerun_parser = subparsers.add_parser("rerun", help="Rerun a workflow")
    add_run_and_rerun_options(rerun_parser)
    rerun_parser.add_argument('-w', '--wdir', type=str, required=True,
                           help="Output folder where data are stored")
    rerun_parser.add_argument('--rerun-incomplete', action="store_true", required=False, default=False,
                           help="Rerun incomplete rules")
    rerun_parser.add_argument('--force-unlock', action="store_true", required=False, default=False,
                           help="Unlock run before rerun it")
    rerun_parser.add_argument('-m', '--merge-batches', action="store_true", required=False, default=False,
                              help="Unlock run before rerun it")
    rerun_parser.set_defaults(func="run_or_rerun")
    rerun_parser.set_defaults(mode="rerun")

    # Unlock command:
    unlock_parser = subparsers.add_parser("unlock", help="Unlock a workflow")
    unlock_parser.add_argument('-w', '--wdir', type=str, required=True,
                              help="Output folder where data are stored")
    unlock_parser.add_argument('--debug', action="store_true", required=False, default=False, help="Enable debug")
    unlock_parser.set_defaults(func="unlock_job")

    # Clean command:
    clean_parser = subparsers.add_parser("clean", help="Clean a workflow")
    clean_parser.add_argument('-w', '--wdir', type=str, required=True, help="Working folder where data is stored")
    clean_parser.add_argument('-f', '--fake', action="store_true", required=False, default=False,
                              help="Do not delete files, only show files which will be deleted")
    clean_parser.add_argument('--debug', action="store_true", required=False, default=False, help="Enable debug")
    clean_parser.set_defaults(func="clean")

    # Soft clean command:
    softclean_parser = subparsers.add_parser("soft-clean", help="Clean logs of a workflow")
    softclean_parser.add_argument('-w', '--wdir', type=str, required=True, help="Working folder where data is stored")
    softclean_parser.add_argument('-f', '--fake', action="store_true", required=False, default=False,
                                  help="Do not delete files, only show files which will be deleted")
    softclean_parser.add_argument('--debug', action="store_true", required=False, default=False, help="Enable debug")
    softclean_parser.set_defaults(func="soft_clean")

    # Test of the pipeline:
    test_parser = subparsers.add_parser("test")
    test_parser.add_argument('-w', '--wdir', type=str, required=True, help="Working folder where data is stored")
    add_run_and_rerun_options(test_parser)
    test_parser.set_defaults(func="run_test")

    args = parser.parse_args()

    try:
        func = args.func
    except AttributeError:
        parser.error("too few arguments use -h for help")

    if args.func == "run_detection" and args.refbundle is None and "genomestrip" in args.tools:
        print("Error: --refbundle option is required for genomestrip")
        exit(1)

    if hasattr(args, "func"):
        exit(_launch(args))
    else:
        parser.print_help()
